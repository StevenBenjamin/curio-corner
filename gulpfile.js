var gulp = require('gulp');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var livereload = require('gulp-livereload');
var minifyCSS = require('gulp-minify-css');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');



gulp.task('default', ['minify-js', 'sass'], function() {
  livereload.listen();
  gulp.watch('./assets/scripts/*/*.js', ['minify-js']);
  gulp.watch(['./assets/stylesheets/partials/*.scss'],
    ['sass', livereload.changed]);
});

// JS
gulp.task('jshint', function(cb) {
  gulp.src('./assets/*.js')
  .pipe(jshint())
  .pipe(jshint.reporter('default'));
  cb();
});

gulp.task('minify-js', ['jshint'], function(cb) {
  gulp.src('./assets/scripts/*/*.js')
    .pipe(concat('site.min.js'))
  	//.pipe(uglify())
  	.pipe(gulp.dest('./assets/scripts/'));
  cb();
});


// SCSS
gulp.task('sass', function (cb) {
  gulp.src(['./assets/stylesheets/partials/style.scss','./assets/stylesheets/partials/responsive.scss','./assets/stylesheets/partials/navMenu2.scss','./assets/stylesheets/partials/bootstrap_extended.scss'])
    //.pipe(sass())
    .pipe(concat('site.css'))
    //.pipe(minifyCSS())
    .pipe(gulp.dest('./assets/stylesheets/'));
  cb();
});

// gulp.task('minify-css', function(cb) {
//   gulp.src('./assets/CSS/*.css')
//   .pipe(concat('site.css'))
// 	.pipe(minifyCSS())
// 	.pipe(gulp.dest('./assets/prod/'));
//   cb();
// });
