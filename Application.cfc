
<cfcomponent>
    
    <!--- Set Application Parameters. --->
	<cfset this.name = hash(getCurrentTemplatePath())>
	<cfset this.sessionManagement = "Yes">
    <cfset this.loginStorage = "Session">
    <cfset this.applicationTimeOut = "#createTimeSpan(30,0,0,0)#">
    <cfset this.sessionTimeout = "#createTimeSpan(0,0,30,0)#">  

    <!--- APPLICATION METHODS --->
	<cffunction name="onApplicationStart" returntype="boolean" output="yes">
        === Application Started G ===<br/>
		<!--- read the configuation file to set the app variables --->
        <cfinclude template="../storesConfig.cfm" />
		<cfset application.config = variables.config />	
        <cfset application.datasource = application.config.dsn />
		<cfreturn true>
	</cffunction>
	
	<cffunction name="onApplicationEnd" returntype="void" output="no">
	</cffunction>
    <!---/ APPLICATION METHODS --->	
	

    <!--- SESSION METHODS --->	
	<cffunction name="onSessionStart" returntype="boolean" output="no">
		<cfreturn true>
	</cffunction>

	<cffunction name="onSessionEnd" returntype="void" output="no">
        
	</cffunction>
            
    <!--- REQUEST METHODS --->
	<cffunction name="onRequestStart" returntype="boolean" output="yes">	
        <!--- <cfdump var="#application.config#" /> --->
        
		<!--- if the appsetting are not cached in the application scope, then reload them. --->
		<cfif NOT isDefined("application.config")>
			<cfset onApplicationStart() />
		</cfif>
		
        
        <!--- Set Application Parameters. --->
        <cfparam name="url.resetApp" default="0">
        <cfparam name="url.appReset" default="0">
        <cfif url.resetApp EQ 1 OR url.appReset EQ 1>
            <cfset onApplicationStart() />
        </cfif>
		
		
		<!--- get the protocol --->
		<cfset request.protocol = "http://" />
		<cfif cgi.SERVER_PORT_SECURE>
			<cfset request.protocol = "https://" />
		</cfif>
		
		<cfif isDefined("url.showAppSettings") AND url.showAppSettings EQ 1>
			<cfdump var="#application.config#">
		</cfif>
			                
        <!--- Set the debug settings from the config txt file. --->
		<cfsetting showdebugoutput="#application.config.showDebug#">
	
		<cfreturn true>
	</cffunction>
		

	<cffunction name="onRequestEnd" returntype="void" output="no">
		<!--- url switch to override debug settings. --->
		<cfif isDefined("url.debug")>
			<cfsetting showdebugoutput="#yesNoFormat(url.debug)#">
		</cfif>
	</cffunction>

	
	
	
	
	<cffunction name="onError" returntype="any" output="yes">	
		<cfargument name="exception" required="true">		

		<!--- display the error on screen if set in the app config file or url switch override. --->
		<cfif application.config.showError OR (isDefined("url.showError") AND url.showError EQ 1) OR 1 EQ 1>
			<!--- Display the error --->
			<cfdump var="#arguments.exception#" label="Error">
			<br/>
			<!--- Display diagnostics Information --->
			<cfdump var="#session#" label="session">
			<br/>
			<cfdump var="#cookie#" label="cookie">
			<br/>
			<cfdump var="#cgi#" label="cgi">
		<cfelse>	
			oops, It looks like something went wrong.<br/>
			We have been automatically notified about this error.<br/>
			Please contact us if you have any additional questions.<br/>
			<br/>
			Steven Benjamin<br/>
			steven@livestorefronts.com<br/>
			872-222-9701<br/>
			Code 234324<br/>
			<cfoutput>#application.config.errorEmail#</cfoutput>
		</cfif>

			
		<cfmail to="#application.config.errorEmail#"
        from="server@#replaceNoCase(CGI.SERVER_NAME,'www.','')#"
        subject="Error: #CGI.HTTP_HOST#"
        type="HTML">
			URL: #cgi.server_name##cgi.script_name#?#cgi.query_string#
            URL: #URLDECODE(CGI.SERVER_NAME)##URLDECODE(CGI.SCRIPT_NAME)#?#URLDECODE(CGI.QUERY_STRING)#
			<br/>
			<br/>	
			<cfif IsDefined("arguments.exception")>
				<span style="color:green">Error Details:</span><hr/>
				arguments.exception CFDUMP:<br />
				<cfdump var="#arguments.exception#" label="arguments.exception">
			</cfif>

			
			<br/>
			<br/>
			<cfif isDefined("cookie")>
				cookie: <br />
				<table border="2" cellspacing="2" cellpadding="2">
					<tr>
						<th>Key:</th>
						<th>Value:</th>
					</tr>
					<cfloop collection="#cookie#" item="Item">
						<cfif cookie[Item] NEQ "">
							<tr>
								<td style="background-color:pink; color:black; font-weight:bold;">#Item#</td>
                                <td><cfdump var="#cookie[Item]#" /></td>
							</tr>
						</cfif>
					</cfloop>	
				</table>
			</cfif>
			
			<br/>
			<br/>
			<cfif isDefined("session")>
				Session: <br />
				<table border="2" cellspacing="2" cellpadding="2">
					<tr>
						<th>Key:</th>
						<th>Value:</th>
					</tr>
					<cfloop collection="#Session#" item="Item">
						<cfif Session[Item] NEQ "">
							<tr>
								<td style="background-color:blue; color:white; font-weight:bold;">#Item#</td>
								<td><cfdump var="#SESSION[Item]#" /></td>
							</tr>
						</cfif>
					</cfloop>	
				</table>
			</cfif>
			
			<br/>
			<br/>	
			CGI:<br/>		
			<table border="2" cellspacing="2" cellpadding="2">
				<tr>
					<th>Key:</th>
					<th>Value:</th>
				</tr>
				<cfloop collection="#CGI#" item="cgiItem">
					<cfif CGI[cgiItem] NEQ "">
						<tr>
							<td bgcolor="##00FFFF">CGI.#cgiItem#</td>
							<td>#CGI[cgiItem]#</td>
						</tr>
					</cfif>
				</cfloop>	
			</table>
		</cfmail>		
			
		<CFRETURN true>
	</cffunction>
	
	<cffunction name="onMissingTemplate" returntype="any" output="yes">
		Missing TEMPLATE:
		
		<cfset request.protocol = "http://" />
		<cfif cgi.SERVER_PORT_SECURE>
			<cfset request.protocol = "https://" />
		</cfif>
	
        <cfmail to="#application.config.adminEmail#,#application.config.errorEmail#"
            from="server@#replaceNoCase(CGI.SERVER_NAME,'www.','')#"
            subject="Mapified Error: Missing Template"
            type="HTML">
                URL: #CGI.SERVER_NAME##CGI.SCRIPT_NAME#?#CGI.QUERY_STRING#
                URL: #URLDECODE(CGI.SERVER_NAME)##URLDECODE(CGI.SCRIPT_NAME)#?#URLDECODE(CGI.QUERY_STRING)#
                <br/>
                <br/>					
                <table border="2" cellspacing="2" cellpadding="2">
                    <tr>
                        <th>Key:</th>
                        <th>Value:</th>
                    </tr>
                    <cfloop collection="#CGI#" item="cgiItem">
                        <cfif CGI[cgiItem] NEQ "">
                            <tr>
                                <td bgcolor="##00FFFF">CGI.#cgiItem#</td>
                                <td>#CGI[cgiItem]#</td>
                            </tr>
                        </cfif>
                    </cfloop>	
                </table>
            </cfmail>

			<!--- Forces a page reload if an old cache is calling an page or asset that was removed --->
			<cfif application.config.showError EQ false>
				<cflocation url="#request.protocol##cgi.http_host#" addtoken="No">
			</cfif>
			
	</cffunction>
	

</cfcomponent>