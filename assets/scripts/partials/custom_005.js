//custom.js

var customScripts = function(){

	var init = function(){
		//liveStore.displaySale(1,10,customScripts.displaySalePopup); //(display sale banner(bool), percent discount (int))
	};

	var displaySalePopup = function(){

		//display a popup for the websites and tablets.
		if(MobileDevice.IsMobileDevice === false || MobileDevice.tabletDevice === true){
			$(".inline").colorbox({inline:true, width:"730px"});
			setTimeout("$.colorbox({href:'notice4.html'})",1500);
			setTimeout("$.colorbox.close()",15000);
		}else{
			//display the static image for mobile phones
			$("#salesBannerPLaceHolder").append('<div id="saleBanner"><img src="assets/images/LaborDay2.jpg" alt="" name="saleBannerImg" id="saleBannerImg" border="0" style="width:100%"></div>');
		};
	};

	return {init				: init,
			displaySalePopup	: displaySalePopup};

}();

//custom utils overrides go here.



