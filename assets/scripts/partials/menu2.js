// on document ready
(function($){
	"use strict";
	$.fx.speeds._default = 500;


	// shopping cart hover
    $('#nav-shopping-button').on('mouseenter',function(){
      $(this).css('z-index','300');
     }).on('mouseleave',function(){
      $(this).css('z-index','189');
     });


	// responsive menu

	window.responsiveMenu = function(){

		var	nav = $('nav[module="navigation"]'),
			header = $('[module="navContainer"]');

		var responsiveMenu = {}

		responsiveMenu.init = function(){
			responsiveMenu.checkWindowSize();
			$(window).on('resize',responsiveMenu.checkWindowSize);
		}

		responsiveMenu.checkWindowSize = function(){

			if($(window).width() < 768){
				responsiveMenu.Activate();
			}
			else{
				responsiveMenu.Deactivate();
			}

		}
		// add click events
		responsiveMenu.Activate = function(){
			if($('html').hasClass('md_touch')) header.off('.touch');
			header.off('click').on('click.responsivemenu','#nav-menu-button',responsiveMenu.openClose);
			header.on('click.responsivemenu','.nav-main-menu li a',responsiveMenu.openCloseSubMenu);
			nav.find('.touch_open_sub').removeClass('touch_open_sub').children('a').removeClass('prevented');
		}
		// remove click events
		responsiveMenu.Deactivate = function(){
			header.off('.responsivemenu');
			nav.removeAttr('style').find('li').removeClass('current_click')
				.end().find('.nav-sub-menu-wrap').removeAttr('style').end().find('.prevented').removeClass('prevented').end()
				.find('.touch_open_sub').removeClass('touch_open_sub');
			$('#nav-menu-button').removeClass('color_grey_light_2').addClass('color_blue');
			if($('html').hasClass('md_touch')) header.off('click').on('click.touch','.nav-main-menu li a',responsiveMenu.touchOpenSubMenu);
		}

		responsiveMenu.openClose = function(){

			$(this).toggleClass('active');
			nav.stop().slideToggle();

		}

		responsiveMenu.openCloseSubMenu = function(e){

			var self = $(this);

			if(self.next('.nav-sub-menu-wrap').length){
				self.parent()
					.addClass('current_click')
					.siblings()
					.removeClass('current_click')
					.children(':not(a)')
					.slideUp();
				self.next().stop().slideToggle();
				self.parent().siblings().children('a').removeClass('prevented');

				if(!(self.hasClass('prevented'))){
					e.preventDefault();
					self.addClass('prevented');
				}else{
					self.removeClass('prevented');
				}
			}

		}

		responsiveMenu.touchOpenSubMenu = function(event){
			var self = $(this);

			if(self.next('.nav-sub-menu-wrap').length){

				if(!(self.hasClass('prevented'))){
					event.preventDefault();
					self.addClass('prevented');
				}else{
					self.removeClass('prevented');
				}

				$(this).parent().toggleClass('touch_open_sub').siblings().removeClass('touch_open_sub').children('a').removeClass('prevented')
					.parent().find('.touch_open_sub').removeClass('touch_open_sub').children('a').removeClass('prevented');

			}

		}

		responsiveMenu.init();
	};

	responsiveMenu();



})(jQuery);

var menuController = function(){

    var setCategoriesView = function(){
        //get the width of the container layout

        var maxWidthMultiplier,
            maxWidth,
            groupULWidth,
            groupWidth,
            w1;

        console.log('menuController.setCategoriesView()');


        if($(window).width() > 768){
            maxWidthMultiplier = .8;
        }
        else{
            return false;
        }

        maxWidth = parseInt($(".mainLayout").width() * maxWidthMultiplier);
        //get the count of ul's in the category menu
        groupULWidth = $(".categoryGroup").first().width();
        groupWidth = $(".categoryGroup").length * groupULWidth;
        w1 = Math.min(groupWidth,maxWidth)



        //if the max container is the min then fit to the ul's
        if (maxWidth === w1){
            w1 = Math.floor(maxWidth/groupULWidth)*groupULWidth;
        }
            console.log(w1);
        $(".categoryContainer").width(w1);
        $(".categoryContainer").css({"opacity":1,"visibility":"visible"});

        //calculate the max room we have for the view
        //find the floor of the ammount that will fit in the view
    };


    return {setCategoriesView:setCategoriesView};
}();







