$(window).resize(function() {
	if(typeof(gWindowWidth) === 'undefined'){
		gWindowWidth = $(window).width();
	}
    if(this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function() {
		//only redraw the products for horizontal changes to the window
		if($(window).width() != gWindowWidth){
			$(this).trigger('resizeEnd');
		}
    }, 500);
});

$(window).bind('resizeEnd', function() {
    //do something, window hasn't changed size in 500ms
    ProductService.resetProductsContainer();
});



 var Search = function(){
    
        var doSearch = function(){
            var searchstr = $("#freeTextSearch").val().trim();
            if(searchstr.length){
               //get an array of all the url params
               var baseUrl = window.location.href.split("?")[0];    
               var queryStr = "";
               var objIdx = 0;    
               var urlParams = getQueryStrings(); 
                console.log('urlParams:',urlParams);
               for (param in urlParams){              
                   if(param != 'searchstr'){
                       if(objIdx === 0){
                           queryStr = '?'+param+'='+urlParams[param];
                       }
                       else{
                            queryStr = queryStr+'&'+param+'='+urlParams[param];
                       };
                       objIdx++;    
                   }; 
               };//loop
                
			   //add the free text search string.	
               if(objIdx === 0){
                    queryStr = queryStr+'?searchstr='+searchstr;
               }
               else{
                    queryStr = queryStr+'&searchstr='+searchstr;
               };    
               
               var searchURL = baseUrl+queryStr;
               window.location.href = searchURL;    
            };
        };
				
		var getSearchStr = function(sParam){
		    sParam = sParam || 'searchstr';
		    var sPageURL = window.location.search.substring(1);
		    var sURLVariables = sPageURL.split('&');
		    for (var i = 0; i < sURLVariables.length; i++) 
		    {
		        var sParameterName = sURLVariables[i].split('=');
		        if (sParameterName[0] == sParam) 
		        {
		            return sParameterName[1];
		        }
				else{
					return "";
				}
		    }
		};
        
        var getQueryStrings = function(){
            var assoc  = {};
            var decode = function (s) { return decodeURIComponent(s.replace(/\+/g, " ")); };
            var queryString = location.search.substring(1); 
            var keyValues = queryString.split('&'); 

            for(var i in keyValues) { 
                var key = keyValues[i].split('=');
                if (key.length > 1) {
                    assoc[decode(key[0])] = decode(key[1]);
                }       
            } 

            return assoc; 
        };
        
        return { doSearch   : doSearch,
                 getQueryStrings : getQueryStrings,
				 getSearchStr : getSearchStr
               }
    
    }(); //Search

// on document ready
var Layout = function(){
    

    var init = function(){
        Popup.init()
        console.log('main.js');
    };
	
	var setProductWidth = function(){
		var containerWidth = parseInt($(".main_layout").width())
		var margins = parseInt($(".product_item").css("margin-left"))*2;
		var productWidth = 250;
		
		if(containerWidth >= 1251){	//screen: >1400
			productWidth = parseInt((containerWidth/7)-margins-2);
			if(productWidth < 215){
				productWidth = parseInt((containerWidth/6)-margins-2);
			}
		}
		else if(containerWidth >= 1190 & containerWidth < 1250){ //screen: 1200 - 1400
			productWidth = parseInt((containerWidth/6)-margins-2);
			if(productWidth < 215){
				productWidth = parseInt((containerWidth/5)-margins-2);
			}
		}
		else if(containerWidth >= 990 & containerWidth < 1189){
			productWidth = parseInt((containerWidth/5)-margins-2);
			if(productWidth < 215){
				productWidth = parseInt((containerWidth/4)-margins-2);
			}
		}
		else if(containerWidth >= 790 & containerWidth < 889){
			productWidth = parseInt((containerWidth/4)-margins-2);
			if(productWidth < 215){
				productWidth = parseInt((containerWidth/3)-margins-2);
			}
		}
		else if(containerWidth >= 590 & containerWidth < 789){
			productWidth = parseInt((containerWidth/3)-margins-2);
			if(productWidth < 185){
				productWidth = parseInt((containerWidth/2)-margins-2);
			}
		}
		else if(containerWidth >= 481 & containerWidth < 589){
			productWidth = parseInt((containerWidth/2.2));
		}
		$(".products_container").show();
		console.warn('DIAGNOSTICS:',containerWidth,productWidth);
		return productWidth;
	};
    
   

    return {init : init,
		   setProductWidth  : setProductWidth}
}(); //layout



// popup  

var Popup = (function(){

    var init = function(){
        console.log('Popup');
        _customEvents();
        _on();
    };

    var _customEvents = function(){
        console.log('_customEvents');
    //reposition the popup to center it on the screen.
        $('.popup').on('ev_centerPopup',function(){
            var _this = $(this),
            pos = setTimeout(repositionPopup,100);
        });
    };

	var repositionPopup = function(){
        console.log('repositionPopup');
		 var winHeight = $(window).innerHeight();
		 var popupHeight = winHeight*.85;
		 var thisPopup = $(".popup");
         var marginLeft = thisPopup.outerWidth() / -2;
         var marginTop = (winHeight-popupHeight+75)*-1;
		 
		 console.log('repositionPopup',winHeight,popupHeight,marginTop);
		  
         thisPopup.css({
             'margin-left': marginLeft,
             'margin-top' : marginTop,
			 'height' : 'auto',
             'min-height' : '519px'
         });
		
		if(typeof(pos) != "undefined"){		
         	clearTimeout(pos);
		}
     };
	
    var _on = function(){
        //display the popup.
        if($('[data-popup]').length){
            $("body").on('click','[data-popup]',function(e){
                console.log('data-popup');
                var popup = $(this).data('popup');

                var	pc = $(popup).find('.popup'); //popup section within the modal div.
                pc.trigger('ev_centerPopup');

                $(popup).fadeIn(afterFade);

                function afterFade(){
                    //add the click listener to the modal background.
                    $(popup).on('click',function(e){
                        if($(e.target).hasClass('popup_wrap')){
                            $(this).fadeOut();
                        }
                    });
                };

                e.preventDefault();
            });
        };

         _onClose();
    }; 
    
    var displayPopup = function(){
        
        console.log('xxx');
        $(".popup").trigger('ev_centerPopup');
        $(".popup_wrap").fadeIn(afterFade);
        
        function afterFade(){
            console.log('xxx');
            //add the click listener to the modal background.
            $(".popup_wrap").on('click',function(e){
                if($(e.target).hasClass('popup_wrap')){
                    $(this).fadeOut();
                }
            });
			
			QuickView.preloadGridThumbs()
			QuickView.onGridThumbs();
        };
        
        _onClose();
     
    };
    
    var _onClose = function(){ 
        console.log('_onClose');
        var close = $('.popup > .close');
        close.on('click',function(){
            $(this).closest('.popup_wrap').fadeOut();
        });
    };
    
    var closePopup = function(){
        console.log('closePopup');
        $('.popup_wrap').fadeOut();
    };
	
	var getDetailsByPostId = function(postId){
        console.log('getDetailsByPostId');
		postId = postId || 0;
		$.ajax({
				type	: 'GET',
				url	 	: 'includes/productDetails.cfm',
				data	: {postId:postId,dsn:gdsn},
				success : _displayProductDetailsPopup,
			  });
			  
		function _displayProductDetailsPopup(respHTML){
			$("#popupContainer").html("").html(respHTML);
			pos = setTimeout(repositionPopup,25);
			displayPopup();
		};		  
	};

    return {
            init        : init,
            displayPopup: displayPopup,
            closePopup  : closePopup,
			getDetailsByPostId	: getDetailsByPostId
           }

})(); //Popup


// quick view carousel

	var QuickView = function(){
		var qvc,qvcsingle;

		var init = function(){
            console.log('QuickView.init');
		};
		
		var onGridThumbs = function(){
			var dataSrc, src;
			console.log('onGridThumbs');
			var primaryImage = $('.popup_wrap .qv_preview > img');
			$('.popup_wrap .thumbsGrid').unbind().on('click','img',function(){
                console.log('xxx');
				src = $(this).parent().data('src');
				primaryImage.attr('src',src);
		  	}); //click listener	
	  	};
		
		var preloadGridThumbs = function(){
			
			var arrayOfImages = [];
			//loop through all the large image references and pre-load.
			$('.popup_wrap .thumbsGrid div span img').each(function(){
				dataSrc = $(this).parent().data('src');
				arrayOfImages.push(dataSrc);
			});
			
			preload(arrayOfImages);
			
			function preload(arrayOfImages) {
			    $(arrayOfImages).each(function(){
			        $('<img/>')[0].src = this;
			        // Alternatively you could use:
			        // (new Image()).src = this;
			    });
			};
		};
		
		return {
				init					: init,
				onGridThumbs			: onGridThumbs,
				preloadGridThumbs		: preloadGridThumbs
			   }
		
	}(); //QuickView





(function($){
	"use strict";

	var globalDfd = $.Deferred();
	$(window).bind('load',function(){
		// after loading all the scripts
		globalDfd.resolve();        
	});

	$(function(){

        $.fx.speeds._default = 500;

        // open dropdown

        $.fn.css3Animate = function(element){
            console.log('fn.css3Animate');
            return $(this).on('click',function(e){
                var dropdown = element;
                $(this).toggleClass('active');
                e.preventDefault();
                if(dropdown.hasClass('opened')){
                    dropdown.removeClass('opened').addClass('closed');
                    setTimeout(function(){
                        dropdown.removeClass('closed')
                    },500);
                }else{
                    dropdown.addClass('opened');
                }
            });
        }


        // waypoints helper functions
        $.fn.waypointInit = function(classN,offset,delay,inv){

        };

        // ie9 placeholder
        (function(){
            if($('html').hasClass('ie9')) {
                $('input[placeholder]').each(function(){
                    $(this).val($(this).attr('placeholder'));
                    var v = $(this).val();
                    $(this).on('focus',function(){
                        if($(this).val() === v){
                            $(this).val("");
                        }
                    }).on("blur",function(){
                        if($(this).val() == ""){
                            $(this).val(v);
                        }
                    });
                });

            }
        })();


        // go to top button
        (function(){
            $('#go_to_top').waypointInit('animate_horizontal_finished','0px',0,true);
            $('#go_to_top').on('click',function(){
                $('html,body').animate({
                    scrollTop : 0
                },500);
            });
        })();

    });
})(jQuery);


	var ProductService = function(){
	
		var Globals = {
			pageIdx : 0,
			appendInProgress : false,
			idStores : 0,
			saleDiscount : 0,
            dsn : gdsn
		};
		
		var init = function(idStores){
			Globals.idStores = idStores;
			console.log('StoreID: ',idStores);
			$(window).scroll(_appendProducts);
			//call isotope two times here.
            applyIsotope(ProductService.centerProducts);
			ProductService.resetProductsContainer();
		};
        
        function resetProductsContainer(){
            $(".products_container").width("auto");
            applyIsotope(ProductService.centerProducts);
        };
        
        function applyIsotope(){
			console.log("window width: ",$(window).width());
			//only use isotope for screen width greater than 480px. (2 locations)
			$(".products_container").width("auto");
			if($(window).width() <=480){
				$(".products_container").show();
				console.log("NO ISOTOPE A",$(window).width())
				removeIsotope();
				return false;
			}else{
				$(".product_item").css({"position":"absolute"});
			};
			
			//set the width of the item before we apply isotope.
			$(".product_item").width(Layout.setProductWidth());
			
			console.log('apply isotope to products');
            if($('.products_container').length){
             
				var container = $('.products_container');			
				container.isotope({
				 	itemSelector : '.product_item',
					layoutMode : 'fitRows'
				});
                
                centerProducts();
			} //if there are products
			gWindowWidth = $(window).width();
        }; //apply Isotope
        
        function centerProducts(){
            
            var prodsContainer = $(".products_container");
            //get the width of the container.
            var containerWidth = prodsContainer.width();
            //get the width of the product containers
            var productItemWidth = $(".product_item").outerWidth(true);
            //get the max number of items in a row.
            var productsPerRow = Math.floor(containerWidth/productItemWidth);
            //set the width of the outer container to center the products.
            prodsContainer.width(productItemWidth*productsPerRow);
			console.log('centerProducts width:',productItemWidth*productsPerRow);
        };
		
		var _appendProducts = function(){
				//if the user scrolled 75% down and an update is not in progress.
				var targetThreshold = parseInt(($(document).height()-$(window).height())*.75);
				if($(window).scrollTop() > targetThreshold && Globals.appendInProgress === false){
					
					$(".appendSpinner").show();
					Globals.appendInProgress = true;
					Globals.pageIdx++;
					_getActiveInventory()
					console.info('GET MORE PRODUCT! ',Globals.pageIdx);
				};
				//console.info('Scrolling Baby:', $(window).scrollTop(),' : ',$(document).height()-$(window).height(),' : ',targetThreshold)
			};
			
			_getActiveInventory = function(resetpageIdx){
				resetpageIdx = resetpageIdx || 'no';
				
				if(resetpageIdx === 'reset'){
					Globals.pageIdx = 0;
				};
				
					
				jQuery.ajax({
					 type	 : 'POST',
					 url	 : './assets/cfc/services2.cfc?method=getActiveInventory',
					 data	 : {
					 			idStores		: Globals.idStores,
							   	searchStr		: Search.getSearchStr('searchstr'),
								categoryId		: Search.getSearchStr('category'),
								pageIdx			: Globals.pageIdx,
                                dsn             : Globals.dsn 
							   },
					success : function(r){_showProductResults(r,resetpageIdx)}
				});
			};
		
		_showProductResults = function(r,resetpageIdx){	
		data = jQuery.parseJSON(r);
		
		
		if(resetpageIdx === 'reset'){
			$("#products").html("");
		};
		
		
		if(!data.success){
			$(".appendSpinner").hide();
			return;
		};
		
		
		//$("#products").html("");
		//index page results. (not search results)

		if(Globals.saleDiscount === 0){
		//no sale inprogress.
		console.log('Loop over the new products.');
			if($(window).width()<480){
				jQuery("#postResults3").tmpl(data.ROWS).appendTo(".products_container");
			}
			else{
				jQuery("#postResults3").tmpl(data.ROWS).appendTo( ".products_append_container");
			};
		}else{
		//sale inprogress.
			if($(window).width()<480){
				jQuery("#postResults3_sale").tmpl(data.ROWS).appendTo(".products_container");
			}
			else{
				jQuery("#postResults3_sale").tmpl(data.ROWS).appendTo( ".products_container");
			};
		};
		
		//add listeners to the new items.
		//onProducts(resetpageIdx);
	
		
		
		
		$(".appendSpinner").hide();
		Globals.appendInProgress = false;
		//setProductColumns(Globals.columns,'load');
		
		//only use isotope for screen width greater than 480px. (2 locations)
		if($(window).width() > 480){
			var container = $('.products_container');
			var newElements = $(".ajaxAdded");
			
			//set the width of the item before we apply isotope.
			$(".product_item").width(Layout.setProductWidth());
			
			//Add the elements and re-render the layout.
			container.append(newElements).isotope('appended',newElements);
			
			//remove the class hook so they don't get added twice.
			newElements.removeClass("ajaxAdded");
		}else{
			if($(window).width() <=480){
				console.log("NO ISOTOPE B");
				removeIsotope();
			};	
		}
		
	};
	
		function removeIsotope(){
			$(".products_container").show();
			//set the product item to 90% of the screen width.
			var productItems = $(".product_item");
			productItems.width(parseInt($(window).width()*.90));
			productItems.css({"position":"relative","transform":"initial"});
		};
		
		return {
			init : init,
            centerProducts : centerProducts,
            resetProductsContainer : resetProductsContainer
		}
	
	}(); //ProductService
