<!--- Author:Steven Ira Benjamin 2 --->
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<cfcomponent extends="utils" hint="service layer">
	<cfsetting showdebugoutput="false">
	<cfset variables.itemsPerPage = 20>	
	
	<cffunction name="init" access="public" returnType="services2" output="false">				
		<cfreturn this>
	</cffunction>
		

	<cffunction name="getActiveInventory" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="idStores" required="yes" />
		<cfargument name="searchStr" default="" />
		<cfargument name="categoryId" default="0" />
		<cfargument name="pageIdx" default="0" />
		<cfargument name="maxRows" default="#variables.itemsPerPage#" />
        <cfargument name="dsn" default="missingDSN_A"/>
        
		

		
		
		<cfset var startRecord = 0>
		<cfset var endRecord = 0>
		<cfset var paginationParams = structNew()>
		
		<cfif arguments.pageIdx EQ 0>
			<cfset startRecord = 0>
			<cfset endRecord = variables.itemsPerPage-1 />
		<cfelse>
			<cfset startRecord = (arguments.pageIdx*variables.itemsPerPage)>
			<cfset endRecord = startRecord+variables.itemsPerPage-1 />
		</cfif>
		
		
		<cfset paginationParams._a_startRecord = startRecord>
		<cfset paginationParams._b_endRecord = endRecord>

		

		<cfif checkHost()>
			<cfset var results=structNew()>
	
			<cfquery name="qry_getPostBySearch" datasource="#arguments.dsn#" cachedwithin="#CreateTimeSpan(0,0,1,0)#">
				SELECT
					p.idposts,
					p.idusers,
					p.price,
					p.subject,
                    p.dateCreated,
                    p.pricequalifier,
                    p.quantity,
                    p.isSale,
					 -- p.thumb as imageName,
					(SELECT imageName from images WHERE isprimary = 1 AND idposts = p.idposts LIMIT 1) as imageName,
				    (SELECT imagePath from images WHERE isprimary = 1 AND idposts = p.idposts LIMIT 1) as imagePath,
					(SELECT imgWidth from images WHERE isprimary = 1 AND idposts = p.idposts LIMIT 1) as imageWidth,
					(SELECT imgHeight from images WHERE isprimary = 1 AND idposts = p.idposts LIMIT 1) as imageHeight,
					(SELECT DATEDIFF(CURRENT_TIMESTAMP,p.dateCreated)) as age,
                    (SELECT showNewBanner FROM store_prefs sp WHERE sp.idstores = p.idstores) as showNewBanner,
					(SELECT newlyArrivedDuration FROM store_prefs sp WHERE sp.idstores = p.idstores) as BannerDuration,
					p.isClearance
				FROM posts p	
				INNER JOIN stores s ON s.idstores = p.idstores
				WHERE p.status = 3
					AND p.idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
					AND dateExpires  > <cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">
					<cfif arguments.searchStr NEQ "">
						AND (p.subject LIKE ('%#arguments.searchStr#%') OR p.descriptions LIKE ('%#arguments.searchStr#%'))
					</cfif>
					<cfif arguments.categoryId GTE 2>
						<!--- AND p.idstoreCategories =  <cfqueryparam value="#arguments.categoryId#" cfsqltype="CF_SQL_INTEGER"> --->
						AND p.idposts IN (
						SELECT idposts 
	                    FROM postsXcategories pXc
	                    WHERE pXc.idstoreCategories = <cfqueryparam value="#arguments.categoryId#" cfsqltype="CF_SQL_INTEGER">)
					</cfif>
					<cfif arguments.categoryId EQ 1>
						AND p.isClearance = 1
					</cfif>
				ORDER BY p.dateCreated desc
				LIMIT #startRecord#,#arguments.maxRows#; 
			</cfquery>

			<cfif qry_getPostBySearch.recordCount GT 0>	
				<cfset results = qry2json(querySet=qry_getPostBySearch,convertLineBreaksForJSON=true,additionalAttributes=paginationParams)>
			<cfelse>
				<cfset results["success"] = false />
				<cfset results["dsn"] =arguments.dsn />
				<cfset results["RECORDCOUNT"] = qry_getPostBySearch.recordCount />
			</cfif>
			
		<cfelse>	
			<cfset results["msg"] = "Invalid Host">
		</cfif>

		<cfreturn results>	
	</cffunction>	
						
	
	
	<cffunction name="getPostDetails" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="postId" type="string" required="yes">
		<cfargument name="idStores" required="yes">
		<cfargument name="dsn" default="missingDSN_L"/>
		
		<cfset var results=structNew()>
		<cfset results["status"] = false>
		<cfset results["checkHost"] = checkHost()>
		
		<cfif checkHost()>
			<cfquery name="qry_getPostDetailsById" datasource="#arguments.dsn#">
				SELECT
					p.idposts,
					p.idusers,
					p.price,
					p.subject,
          			(SELECT CONCAT(p.descriptions,sp.policy)) as descriptions
				FROM posts p
          		INNER JOIN store_prefs sp ON sp.idstores = p.idstores
				WHERE p.idposts = <cfqueryparam value="#arguments.postId#" cfsqltype="CF_SQL_INTEGER">
          		AND p.idstores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>
			<cfset results = qry2json(querySet=qry_getPostDetailsById,convertLineBreaksForJSON=false)>
		</cfif>

		<cfreturn results>	
	</cffunction>
	
	
	<cffunction name="getPostImages" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="postId" type="string" required="yes">
		<cfargument name="dsn" default="missingDSN_L"/>
		<cfset var results=structNew()>
		<cfset results["status"] = false>
		
		<cfif checkHost()>
			<cfquery name="qry_getPostImagesById" datasource="#arguments.dsn#">				
				SELECT
						images.imageName,
						images.imagePath,
						images.imgWidth,
						images.imgHeight,
		         (SELECT MAX( images.imgHeight)  FROM    images   WHERE  images.idposts = #arguments.postId#) as maxHeight,
		         (SELECT MAX( images.imgWidth)  FROM    images   WHERE  images.idposts = #arguments.postId#) as maxWidth
						FROM images
						WHERE images.idposts = #arguments.postId#
						AND isDeleted = 0;
			</cfquery>
			<cfset results = qry2json(qry_getPostImagesById)>
		</cfif>

		<cfreturn results>	
	</cffunction>
	
	
	
	
	
	<cffunction name="getCategoriesByStoreId" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="idStores" required="yes" />
		<cfargument name="dsn" default="missingDSN_K"/>
				
		<cfset var results=structNew()>
		
		<cfif checkHost()>
			
			<!--- get all the distinct custom categories for the items in stock --->	
			<cfquery name="qry_getCategoriesByStoreId" datasource="#arguments.dsn#">
				SELECT
					idstoreCategories,
					categoryDesc
				FROM storecategories
				WHERE idstores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
				AND isActive = <cfqueryparam value="1" cfsqltype="CF_SQL_INTEGER">
				ORDER BY sortOrder ASC, categoryDesc ASC;
			</cfquery>
			
			<!--- Check if there are any items on Clearance --->
			<cfquery name="qry_isClearanceByStoreId" datasource="#arguments.dsn#">
				SELECT idposts
				FROM posts
				WHERE status = 3
				AND dateExpires > CURRENT_TIMESTAMP
				AND idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
				AND isClearance = <cfqueryparam value="1" cfsqltype="CF_SQL_TINYINT">
			</cfquery>
			
			
			<cfif qry_isClearanceByStoreId.recordCount EQ 0>
				<cfset results["showClearance"] = false />
			<cfelse>
				<cfset results["showClearance"] = true />
			</cfif>
			
			<cfset results["customCategories"] = qry2json(qry_getCategoriesByStoreId)>
		<cfelse>	
			<cfset results["auth"] = false>
		</cfif>
		
		<cfreturn results>	
	</cffunction>
	
	

	
	
	
	<!--- Used for the storefronts pages --->
	<cffunction name="getLastUpdatedTimeDiff" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="idStores" required="yes" />
		<cfargument name="dsn" default="missingDSN_J"/>
		
		
		<cfif checkHost()>
			<cfset var qry_getLastUpdatedTimeDiff = ""/>
			<cfset var results=structNew()>
	
			<cfquery name="qry_getLastUpdatedTimeDiff" datasource="#arguments.dsn#">
				SELECT CEIL(MIN(TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP,dateCreated))/60)) as minutesAgo 
				FROM posts p
				WHERE p.status = 3
					AND p.idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
					AND p.dateExpires  > <cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">
			</cfquery>

			<cfif qry_getLastUpdatedTimeDiff.minutesAgo NEQ "">	
				<cfset results = qry2json(querySet=qry_getLastUpdatedTimeDiff)>
			<cfelse>
				<cfset results["success"] = false />
				<cfset results["RECORDCOUNT"] = 0 />
			</cfif>
			
		<cfelse>	
			<cfset results = false>
		</cfif>

		<cfreturn results>	
	</cffunction>	
	
	
	<!--- Unknown Uses --->
	<cffunction name="getLastUpdatedTimeDiffJSON" access="remote" returntype="any" returnformat="plain" output="no">
		<cfargument name="idStores" required="yes" />
		<cfargument name="callback" required="yes" />
		<cfargument name="dsn" default="missingDSN_J"/>
		
		<cfset var qry_getLastUpdatedTimeDiff = ""/>
		<cfset var dataPacket= "">
		<cfset var results=structNew()>
		
		<cfquery name="qry_getLastUpdatedTimeDiff" datasource="#arguments.dsn#">
			SELECT CEIL(MIN(TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP,dateCreated))/60)) as minutesAgo 
			FROM posts p
			WHERE p.status = 3
				AND p.idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
				AND p.dateExpires  > <cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">
		</cfquery>

		<cfif qry_getLastUpdatedTimeDiff.minutesAgo NEQ "">	
			<!--- <cfset dataPacket = qry2json(querySet=qry_getLastUpdatedTimeDiff)> --->
			
			
			<cfset dataPacket = serializeJSON(dataPacket)/>
	
			<cfset data = arguments.callback & "(" & dataPacket & ")">
			 <cfreturn data>
		<cfelse>
			<cfset results["success"] = false />
			<cfset results["RECORDCOUNT"] = 0 />
			<cfreturn results>	
		</cfif>
	

		
	</cffunction>	
	
		
		
		
	<cffunction name="getStoreMetaById" access="remote" returntype="any" returnformat="json" output="yes">
		<cfargument name="idStores" required="yes" />

		<cfif checkHost()>
			<cfset var results=structNew()>
	
	
			<cfquery name="qGetStoreMetaById" datasource="#arguments.dsn#" cachedwithin="#CreateTimeSpan(0, 1, 0, 0)#">
				SELECT
					sp.seoKeywords as metaKeyWords,
					sp.seoDesc as metaDesc,
					s.storeName,
					s.address,
					s.addr1,
					s.addr2,
					s.city,
					s.state,
					s.zip,
					s.phone,
					s.email,
					s.mapLink,
					s.logoImg,
					s.mapImg,
					s.storeImg,
					s.homePage
				FROM stores s
        		INNER JOIN store_prefs sp ON sp.idstores = s.idstores
				WHERE s.idstores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
				AND s.isActive = <cfqueryparam value="1" cfsqltype="CF_SQL_INTEGER">
			</cfquery>
	

			<cfif qGetStoreMetaById.recordCount GT 0>	
				<cfset results = qry2json(querySet=qGetStoreMetaById,convertLineBreaksForJSON=true,lcase=true)>
			<cfelse>
				<cfset results["success"] = false />
				<cfset results["RECORDCOUNT"] = qGetStoreMetaById.recordCount />
			</cfif>
			
		<cfelse>	
			<cfset results = false>
		</cfif>

		<cfreturn results>	
	</cffunction>	
	
	<!--- http://www.livestorefronts.com/demo/cfc/services.cfc?method=formatTimeDiff&MINUTESAGO=2 --->	
	<cffunction name="formatTimeDiff" access="remote" returntype="string">
		<cfargument name="minutesAgo" required="yes">
		
		<cfscript>

			var formatedString = "";

		
			if(minutesAgo < 60){
				//minutes
				
				if(minutesAgo EQ 1){
					formatedString = minutesAgo&' Minute Ago';
				}
				else{
					formatedString = minutesAgo&' Minutes Ago';
				};
			}
			
			else if(minutesAgo GTE 60 AND minutesAgo LT 1440){
				//hours
				var hoursAgo = Int(minutesAgo/60);
				if(hoursAgo EQ 1){
					formatedString = hoursAgo&' Hour Ago';
				}
				else{
					formatedString = hoursAgo&' Hours Ago';
				};
			}
			
			else if(minutesAgo GTE 1440 AND minutesAgo LT 10080){
				//days
				var daysAgo = Int(minutesAgo/60/24);
				if(daysAgo EQ 1){
					formatedString = daysAgo&' Day Ago';
				}
				else{
					formatedString = daysAgo&' Days Ago';
				};
			}else{
				formatedString = '';
			};
			
			
		</cfscript>
		
		<cfreturn formatedString> 

	</cffunction>
		
		

		

	<!--- Used for the placards --->
	<cffunction name="getLastUpdatedJsonp" access="remote" returntype="struct" returnformat="json" output="no">
	<cfargument name="idStores" required="yes" />


		<cfif checkHost()>
			<cfset var qry_getLastUpdatedTimeDiff = ""/>
			<cfset var results=structNew()>
		
			<cfquery name="qry_getLastUpdatedTimeDiff" datasource="#arguments.dsn#">
				SELECT CEIL(MIN(TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP,dateCreated))/60)) as minutesAgo 
				FROM posts p
				WHERE p.status = 3
					AND p.idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
					AND p.dateExpires  > <cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">
			</cfquery>
		
			<cfif qry_getLastUpdatedTimeDiff.minutesAgo NEQ "">	
				<cfset results["success"] = true />
				<cfset results["timeInt"] = qry_getLastUpdatedTimeDiff.minutesAgo />
				<cfset results["timeString"] = formatTimeDiff(qry_getLastUpdatedTimeDiff.minutesAgo) />
			<cfelse>
				<cfset results["success"] = false />
				<cfset results["RECORDCOUNT"] = 0 />
			</cfif>
			
		<cfelse>	
			<cfset results = false>
		</cfif>
		
		<cfreturn results>	
	</cffunction>	

	<!--- Used for the placards --->
	<!--- http://www.livestorefronts.com/demo/cfc/services.cfc?method=getStoreURLByStoreId&idStores=2 --->	
	<cffunction name="getStoreURLByStoreId" access="remote" returntype="struct" returnformat="json" output="no">
		<cfargument name="idStores" required="yes" />
		<cfargument name="dsn" default="missingDSN_I"/>

		
		<cfif checkHost()>
			<cfset var thisStoreURL = ""/>
			<cfset var results=structNew()>
			
		
			<cfquery name="qry_getStoreURLById" datasource="#arguments.dsn#">
				SELECT storeURL,type
				FROM stores
				WHERE idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>
			

		
			<cfif qry_getStoreURLById.storeURL NEQ "">	
				<!--- Send back the url to link to: --->
				<cfset results["storeURL"] = qry_getStoreURLById.storeURL>
				
				<!--- send back the reference to the placard background image --->
				<cfswitch expression="#qry_getStoreURLById.type#">
					<cfcase value="1">
						<!--- Inventory was updated within a week --->
						<cfset results["BGimg"] = "last_updated" />
					</cfcase>
					<cfcase value="2">
						<cfset results["BGimg"] = "ms_LastUpdated_" />
					</cfcase>
					<cfdefaultCase>
						<cfset results["BGimg"] = "last_updated" />
					</cfdefaultcase>
				</cfswitch>
				
				
			<cfelse>
				<cfset results["success"] = false />
				<cfset results["RECORDCOUNT"] = 0 />
			</cfif>
			
		<cfelse>	
			<cfset results = false>
		</cfif>
		
		<cfreturn results>	
	</cffunction>	
		

		

	<cffunction name="remoteGetLastUpdatedByStoreId" access="remote" returnType="any" returnFormat="plain" output="false">
		<cfargument name="callback" type="string" required="false">
		<cfargument name="storeId" default="0">
		
			
		<cfif arguments.storeId EQ 0>
			<cfreturn>
		</cfif>
		
		
		<cfset var results = structNew() />
		
		<cfset var ageValues = getLastUpdatedJsonp(arguments.storeId) />
		<cfset results["store"] = getStoreURLByStoreId(arguments.storeId)>	

		
		<cfif ageValues.success>
			<cfset results["success"] = true />
			<cfset results["timeLastUpdated"] = ageValues.timeString>
		
			<!--- if the store or garagesale was update more than a week ago. --->
			<!--- Then override the placard background image with a generic one --->
			<cfif ageValues.timeInt GTE 10080>
				<cfset results["store"]["BGimg"] = "1w_updated" />
			</cfif>
		<cfelse>	
			<cfset results["success"] = false />
			<cfset results["timeLastUpdated"] = "" />
			<cfset results["store"]["BGimg"] = "" />
		</cfif>

		
		

		
		<!--- serialize --->
		<cfset results = serializeJSON(results)>
		
		<!--- jsonp wrap --->
		<cfif structKeyExists(arguments, "callback")>
			<cfset results = arguments.callback & "(" & results & ")">
		</cfif>
		
		<cfreturn results>
	</cffunction>	
	
	<cffunction name="getPostByPostId" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="itemId" required="yes" />
		<cfargument name="dsn" default="missingDSN_H"/>
		
		
		<cfquery name="qry_getPostById" datasource="#arguments.dsn#">
			SELECT
				p.idposts,
				p.idusers,
				p.price,
				p.subject,
				-- p.thumb as imageName,
				(SELECT imageName from images WHERE isprimary = 1 AND idposts = p.idposts LIMIT 1) as imageName,
				(SELECT imagePath from images WHERE imageName = p.thumb AND idposts = p.idposts LIMIT 1) as imagePath,
				(SELECT imgWidth from images WHERE imageName = p.thumb AND idposts = p.idposts LIMIT 1) as imageWidth,
				(SELECT imgHeight from images WHERE imageName = p.thumb AND idposts = p.idposts LIMIT 1) as imageHeight,
				(SELECT DATEDIFF(CURRENT_TIMESTAMP,p.dateCreated)) as age,
				(SELECT IF( (DATEDIFF(CURRENT_TIMESTAMP,p.dateCreated) <= sp.newlyArrivedDuration AND sp.showNewBanner = 1) ,true,false)) as showNewBanner,
				p.isClearance
			FROM posts p	
			INNER JOIN store_prefs sp ON sp.idstores = p.idstores
			INNER JOIN stores s ON s.idstores = p.idstores
			WHERE p.IDPOSTS = <cfqueryparam value="#arguments.itemId#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>

		<cfif qry_getPostById.recordCount GT 0>	
			<cfset results = qry2json(querySet=qry_getPostById,convertLineBreaksForJSON=true)>
		<cfelse>
			<cfset results["success"] = false />
			<cfset results["dsn"] = arguments.dsn />
			<cfset results["RECORDCOUNT"] = qry_getPostById.recordCount />
		</cfif>
		<cfreturn results>	
	</cffunction>

	<cffunction name="TestDumpQuery" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="dsn" default="missingDSN_G"/>
		<cfquery name="qry_getPostBySearch" datasource="#arguments.dsn#">
			SELECT * from posts LIMIT 0,2
		</cfquery>
		<cfdump var="#qry_getPostBySearch#"><cfabort>
	</cffunction>
	
	<!--- add post to cart list begin --->
	<cffunction name="AddPostToCartList" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="itemId" required="yes" />
		<cfargument name="quantity" required="yes" />
		<cfset variables.isExist = false >
	
		<cfif not structkeyexists(session,"cart")>       	
			<cfset session.cart = ArrayNew(2)>
		</cfif>
		<cfif arguments.quantity >
			<cfloop index="i" from="1" to="#ArrayLen(session.cart)#">
				<cfif (ArrayContains(session.cart[i],arguments.itemid))>
					<cfset session.cart[i][1]= arguments.quantity>
					<cfset variables.isExist = true >
				</cfif>
			</cfloop>
			<cfif not variables.isExist>
				<cfset variable.newitemIndex = ArrayLen(session.cart)+1>		
				<cfset session.cart[i][1] = arguments.quantity>					
				<cfset session.cart[variable.newitemIndex][2] = arguments.itemid>
			</cfif> 
		<cfelse>
			<cfset deleteCartItem(arguments.itemId) >
		</cfif>
		<cfreturn ArrayLen(session.cart)>
	</cffunction>
	<!--- add post to cart list end --->
	
	<!--- Delete cart list item begin --->
	<cffunction name="deleteCartItem" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="itemId" required="yes" />
		<cfset variables.arrayIndex = 1 >
		<cfloop array="#session.cart#" index="index">
			<cfif ArrayFind(index,arguments.itemId) >
				<cfset temp = ArrayFind(index,arguments.itemId) >
				<cfset ArrayDeleteAt(session.cart, variables.arrayIndex) >
			</cfif>
			<cfset variables.arrayIndex ++>
		</cfloop>
		<cfreturn ArrayLen(session.cart)>
	</cffunction>
	<!--- Delete cart list item end --->
	
	<cffunction name="getAddedCartItems" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="dsn" default="missingDSN_F"/>
		<cfset myQuery = QueryNew("IDPOSTS, IDUSERS, AGE, PRICE, SUBJECT , IMAGENAME,IMAGEHEIGHT, IMAGEWIDTH, IMAGEPATH, ISCLEARANCE, PATH, SHOWNEWBANNER, QUANTITY, TOTALPRICE", "Integer, Integer, Integer, Double, VarChar, VarChar, Integer, Integer, VarChar, Integer, VarChar, Integer, Integer, Double")>
		
		<!---<cfset variables.cartItems = "" >--->
		<cfset variables.TotalPrice = 0 >
		<cfloop array="#session.cart#" index="index">
			<!---<cfset variables.cartItems = ListAppend( variables.cartItems, index[2]) >--->
			<cfset newRow = QueryAddRow(MyQuery)>
			<cfquery name="qry_getPostByCartItemId" datasource="#arguments.dsn#">
				SELECT
					p.idposts,
					p.idusers,
					p.price,
					p.subject,
					-- p.thumb as imageName,
					(SELECT imageName from images WHERE isprimary = 1 AND idposts = p.idposts LIMIT 1) as imageName,
					(SELECT imagePath from images WHERE imageName = p.thumb AND idposts = p.idposts LIMIT 1) as imagePath,
					(SELECT imgWidth from images WHERE imageName = p.thumb AND idposts = p.idposts LIMIT 1) as imageWidth,
					(SELECT imgHeight from images WHERE imageName = p.thumb AND idposts = p.idposts LIMIT 1) as imageHeight,
					(SELECT DATEDIFF(CURRENT_TIMESTAMP,p.dateCreated)) as age,
					(SELECT IF( (DATEDIFF(CURRENT_TIMESTAMP,p.dateCreated) <= sp.newlyArrivedDuration AND sp.showNewBanner = 1) ,true,false)) as showNewBanner,
					p.isClearance
				FROM posts p	
				INNER JOIN store_prefs sp ON sp.idstores = p.idstores
				INNER JOIN stores s ON s.idstores = p.idstores
				WHERE p.IDPOSTS IN (#index[2]#)
			</cfquery>
			
				<cfset variables.TotalPrice = index[1] * val(qry_getPostByCartItemId.PRICE) >
			
			<cfset temp = QuerySetCell(myQuery, "IDPOSTS", "#qry_getPostByCartItemId.IDPOSTS#", newRow)>
			<cfset temp = QuerySetCell(myQuery, "IDUSERS", "#qry_getPostByCartItemId.IDUSERS#", newRow)>
			<cfset temp = QuerySetCell(myQuery, "AGE", "#qry_getPostByCartItemId.AGE#", newRow)>
			<cfset temp = QuerySetCell(myQuery, "PRICE", "#qry_getPostByCartItemId.PRICE#", newRow)>
			<cfset temp = QuerySetCell(myQuery,"SUBJECT", "#qry_getPostByCartItemId.SUBJECT#", newRow)>
			<cfset temp = QuerySetCell(myQuery, "IMAGENAME", "#qry_getPostByCartItemId.IMAGENAME#", newRow)>
			
			<cfset temp = QuerySetCell(myQuery, "IMAGEHEIGHT", "#qry_getPostByCartItemId.IMAGEHEIGHT#", newRow)>
			<cfset temp = QuerySetCell(myQuery, "IMAGEWIDTH", "#qry_getPostByCartItemId.IMAGEWIDTH#", newRow)>
			<cfset temp = QuerySetCell(myQuery, "IMAGEPATH", "#qry_getPostByCartItemId.IMAGEPATH#", newRow)>
			<cfset temp = QuerySetCell(myQuery,"ISCLEARANCE", "#qry_getPostByCartItemId.ISCLEARANCE#", newRow)>
			<cfset temp = QuerySetCell(myQuery, "PATH", "#qry_getPostByCartItemId.PATH#", newRow)>
			<cfset temp = QuerySetCell(myQuery, "SHOWNEWBANNER", "#qry_getPostByCartItemId.SHOWNEWBANNER#", newRow)>
			<cfset temp = QuerySetCell(myQuery,"QUANTITY", "#index[1]#", newRow)>
			<cfset temp = QuerySetCell(myQuery,"TOTALPRICE", "#variables.TotalPrice#", newRow)>
			
		</cfloop>
		
	
		<cfif MyQuery.recordCount GT 0>	
			<cfset results = qry2json(querySet=MyQuery,convertLineBreaksForJSON=true)>
		<cfelse>
			<cfset results["success"] = false />
			<cfset results["dsn"] =arguments.dsn />
			<cfset results["RECORDCOUNT"] = MyQuery.recordCount />
			
		</cfif>
		<cfreturn results>
	</cffunction>
	
	
	<cffunction name="getAllCategoriesByStoreId" access="remote" returntype="any" returnformat="json" output="no">
		
		<cfargument name="dsn" default="missingDSN_E"/>
		<cfargument name="postId" default="0">
		<cfargument name="storeId" required="yes" />
		
		<cfset var storeCategories = arrayNew(1)/>
		
		
		<cfquery name="getAllCategoriesByStoreId" datasource="#arguments.dsn#">
			
			SELECT
				sc.idstoreCategories,
				sc.categoryDesc,  
	        	(SELECT COUNT(pXc.idposts)
					FROM postsXcategories pXc 
					WHERE pXc.idstoreCategories = sc.idstoreCategories 
					AND pXc.idposts = <cfqueryparam value="#arguments.postId#" cfsqltype="CF_SQL_INTEGER">) as hasCategory
			FROM storecategories sc
			WHERE sc.idstores = <cfqueryparam value="#arguments.storeId#" cfsqltype="CF_SQL_INTEGER">
			AND isActive = 1
			ORDER BY sc.categoryDesc ASC;
				
		</cfquery>
		
		<cfset results = qry2json(getAllCategoriesByStoreId)>
		<cfreturn results>	
	</cffunction>
	
	<cffunction name="emailSubscriptionCnfrm" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="emailSub" default="0">
		<cfargument name="dsn" default="missingDSN_D"/>
		<cfquery name="emailSubscriptionCnfrm" datasource="#arguments.dsn#">
			SELECT *
			FROM emailsubscription
			WHERE email = <cfqueryparam value="#arguments.emailSub#" cfsqltype="cf_sql_varchar">
		</cfquery>
		<cfif emailSubscriptionCnfrm.recordcount >
			<cfset results = "true" >
		<cfelse>
			<cfquery name="emailSubscriptionCnfrm" datasource="#arguments.dsn#">
				INSERT INTO emailsubscription(EMAIL,SUBSCRIPTION_ID)
				VALUES (
				<cfqueryparam value="#arguments.emailSub#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="1" cfsqltype="cf_sql_integer">
				)
			</cfquery>
			<cfset results = "false" >
		</cfif>
		<cfreturn results>	
	</cffunction>
	
	<cffunction name="unsubscribeEmail" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="emailSub" default="0">
		<cfargument name="dsn" default="missingDSN_C"/>
		<cfquery name="unsubscribeEmail" datasource="#arguments.dsn#">
			DELETE 
			FROM emailsubscription 
			WHERE email = <cfqueryparam value="#arguments.emailSub#" cfsqltype="cf_sql_varchar">
		</cfquery>
	</cffunction>
	
	<!--- for testing functions only --->
	<cffunction name="testQuery" access="remote" returntype="any">
			<cfargument name="dsn" default="missingDSN_B"/>
			<cfquery name="qry_getPostByCartItemId" datasource="#arguments.dsn#">
				<!---SHOW TABLES FROM #arguments.dsn#	--->
				select * from emailsubscription
				
			</cfquery>
			
			<cfdump var="#qry_getPostByCartItemId#"><cfabort>
	</cffunction>
	<!--- for testing functions only --->
	
</cfcomponent>