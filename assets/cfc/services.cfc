<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<cfcomponent extends="utils" hint="service layer">
	<cfsetting showdebugoutput="false">
	<cfset variables.itemsPerPage = 25>	
        
        
    <!--- add update and remove items from cart --->    
    <cffunction name="shoppingCartMgr" access="remote" returntype="any" output="no" returnFormat="json">
		<cfargument name="storeId" type="any" required="no">
        <cfargument name="productId" type="any" required="no">
        <cfargument name="desc" type="any" required="no" default="">
        <cfargument name="sessionId" type="any" required="no">
        <cfargument name="qty" type="any" required="no">
            
            
  
        
             
        <cfset var results = structNew()>
                
            <!--- get the price from the database --->
            
            <!--- is this a small, large or single priced item --->
            <cfif RIGHT(arguments.productId,2) EQ "SM">
                <cfquery name="qry_getPriceBypostId" datasource="#application.config.dsn#">
                    SELECT small_price as price
                    FROM posts
                    WHERE idposts = <cfqueryparam value="#arguments.productId#" cfsqltype="cf_sql_varchar">
                </cfquery>   
            <cfelseif RIGHT(arguments.productId,2) EQ "LG">   
                <cfquery name="qry_getPriceBypostId" datasource="#application.config.dsn#">
                    SELECT large_price as price
                    FROM posts
                    WHERE idposts = <cfqueryparam value="#arguments.productId#" cfsqltype="cf_sql_varchar">
                </cfquery> 
            <cfelse>    
                <cfquery name="qry_getPriceBypostId" datasource="#application.config.dsn#">
                    SELECT price
                    FROM posts
                    WHERE idposts = <cfqueryparam value="#arguments.productId#" cfsqltype="cf_sql_varchar">
                </cfquery> 
            </cfif>
            
            
  
                
            <!--- check if the cart exists and it is not older than 30 minutes + --->
            <cfquery name="qry_cartExists" datasource="#application.config.dsn#">
                SELECT idshoppingCart
                FROM shoppingcartstatemgr
                WHERE userSessionId = <cfqueryparam value="#arguments.sessionId#" cfsqltype="CF_SQL_VARCHAR">
                AND idstore = <cfqueryparam value="#arguments.storeId#" cfsqltype="CF_SQL_INTEGER">
                AND MINUTE(TIMEDIFF(CURRENT_TIMESTAMP,dateCreated)) < 30
                AND checkoutcompleted = 0;
            </cfquery>   
                
            <!--- Create a new shopping cart and insert the new item --->    
            <cfif NOT qry_cartExists.recordCount>
                <cfquery name="qry_createCart" datasource="#application.config.dsn#" result="newCart">
                    INSERT INTO shoppingcartstatemgr
                        (
                          idstore,
                          userSessionId
                        )
                        VALUES
                        (
                          <cfqueryparam value="#arguments.storeId#" cfsqltype="CF_SQL_INTEGER">,
                          <cfqueryparam value="#arguments.sessionId#" cfsqltype="CF_SQL_VARCHAR">
                        );
                </cfquery>
                              
                <cfset var shoppingCartId = newCart.GENERATEDKEY />
                              
                <cfquery name="ins_cartItem" datasource="#application.config.dsn#">
                    INSERT INTO shoppingcartitems
                    (
                      idshoppingcart,
                      idpost,
                      itemdescription,
                      qty,
                      price
                    )
                    VALUES
                    (
                      <cfqueryparam value="#newCart.GENERATEDKEY#" cfsqltype="CF_SQL_INTEGER">,
                      <cfqueryparam value="#arguments.productId#" cfsqltype="cf_sql_varchar">,
                      <cfqueryparam value="#arguments.desc#" cfsqltype="CF_SQL_VARCHAR">,
                      <cfqueryparam value="#arguments.qty#" cfsqltype="CF_SQL_INTEGER">,
                      <cfqueryparam value="#qry_getPriceBypostId.price#" cfsqltype="CF_SQL_FLOAT">
                    );  
                </cfquery>    
                          
            <cfelse>
                <!--- cart exist --->
                <cfset var shoppingCartId = qry_cartExists.idshoppingCart />  
                
                <!--- update the cart session datetime stamp --->
                <cfquery name="upd_cartSession" datasource="#application.config.dsn#">
                    UPDATE shoppingcartstatemgr
                    SET dateCreated = CURRENT_TIMESTAMP
                    WHERE userSessionId = <cfqueryparam value="#arguments.sessionId#" cfsqltype="CF_SQL_VARCHAR">
                    AND idstore = <cfqueryparam value="#arguments.storeId#" cfsqltype="CF_SQL_INTEGER">;
                </cfquery>
                
                <!--- are we updating an existing item? --->
                <cfquery name="chk_cartItemById" datasource="#application.config.dsn#">
                    SELECT idshoppingcartitems
                    FROM shoppingcartitems
                    WHERE idshoppingcart = <cfqueryparam value="#shoppingCartId#" cfsqltype="CF_SQL_INTEGER">
                    AND idpost = <cfqueryparam value="#arguments.productId#" cfsqltype="cf_sql_varchar">
                    <cfif LEN(arguments.desc)>
                        AND itemdescription = <cfqueryparam value="#arguments.desc#" cfsqltype="CF_SQL_VARCHAR">
                    </cfif>
                </cfquery>
                    
             
                    
                <cfif chk_cartItemById.recordCount EQ 0>
                    
                    <!--- else update it --->
                    <cfquery name="ins_cartItem" datasource="#application.config.dsn#">
                        INSERT INTO shoppingcartitems
                        (
                          idshoppingcart,
                          idpost,
                          itemdescription,
                          qty,
                          price
                        )
                        VALUES
                        (
                          <cfqueryparam value="#shoppingCartId#" cfsqltype="CF_SQL_INTEGER">,
                          <cfqueryparam value="#arguments.productId#" cfsqltype="cf_sql_varchar">,
                          <cfqueryparam value="#arguments.desc#" cfsqltype="CF_SQL_VARCHAR">,
                          <cfqueryparam value="#arguments.qty#" cfsqltype="CF_SQL_INTEGER">,
                          <cfqueryparam value="#qry_getPriceBypostId.price#" cfsqltype="CF_SQL_FLOAT">
                        );  
                    </cfquery>
               
                <cfelse><!--- item already exist in cart --->
                    
                    <!--- if the qty is 0 then remove the item --->
                    <cfif arguments.qty EQ 0>
                    
                        <cfquery name="del_cartItem" datasource="#application.config.dsn#">
                            DELETE FROM shoppingcartitems
                            WHERE idshoppingcart = <cfqueryparam value="#shoppingCartId#" cfsqltype="CF_SQL_INTEGER">
                            AND idpost = <cfqueryparam value="#arguments.productId#" cfsqltype="cf_sql_varchar">
                            <cfif LEN(arguments.desc)>
                                AND itemdescription = <cfqueryparam value="#arguments.desc#" cfsqltype="CF_SQL_VARCHAR">
                            </cfif>
                        </cfquery>
                        
                    <cfelse>
                    
                        <cfquery name="upd_cartItem" datasource="#application.config.dsn#" result="updateSQL">
                            UPDATE shoppingcartitems
                            SET qty = <cfqueryparam value="#arguments.qty#" cfsqltype="CF_SQL_INTEGER">
                            WHERE idshoppingcart = <cfqueryparam value="#shoppingCartId#" cfsqltype="CF_SQL_INTEGER">
                            AND idpost = <cfqueryparam value="#arguments.productId#" cfsqltype="cf_sql_varchar">
                            AND itemdescription = <cfqueryparam value="#arguments.desc#" cfsqltype="CF_SQL_VARCHAR">
                        </cfquery>
                            
                        <!--- <cfdump var="updateSQL" abort/>    --->
                            
                    </cfif>        
                        
                </cfif><!--- item exist or not --->    
                
            </cfif><!--- cart exist or not --->
                                    
            <!--- get the current status of the shopping cart and return the data to the client --->
            <cfquery name="getCartById" datasource="#application.config.dsn#">
                SELECT     
					concat('$', format(SUM(price*qty), 2)) as total,
                    SUM(qty) as itemsincart
                FROM shoppingcartitems
                WHERE idshoppingcart = <cfqueryparam value="#shoppingCartId#" cfsqltype="CF_SQL_INTEGER">
            </cfquery>              
            
            <cfset results.shoppingCartSessionId = shoppingCartId /> <!--- diagnostics: remove for production --->
            <cfset results.qty =  getCartById.itemsincart />
            <cfset results.total = getCartById.total />   
                
        <cfreturn results />
    </cffunction>
                        
              
                  
	
    <cffunction name="getShoppingCartSummary" access="remote" returntype="any" output="no" returnFormat="json">
        <cfargument name="storeId" type="any" required="no">
        <cfargument name="sessionId" type="any" required="no"> 
            
        <cfset var results = structNew()>
            
        <!--- check if the cart exists and it is not older than 3 minutes + --->
        <cfquery name="qry_cartExists" datasource="#application.config.dsn#">
            SELECT idshoppingCart
            FROM shoppingcartstatemgr
            WHERE userSessionId = <cfqueryparam value="#arguments.sessionId#" cfsqltype="CF_SQL_VARCHAR">
            AND idstore = <cfqueryparam value="#arguments.storeId#" cfsqltype="CF_SQL_INTEGER">
            AND MINUTE(TIMEDIFF(CURRENT_TIMESTAMP,dateCreated)) < 30
            AND checkoutcompleted = 0;
        </cfquery> 
            
        <cfif qry_cartExists.recordCount EQ 0>
            <cfset results.qty = 0 />
            <cfset results.total = "$0.00" />
        <cfelse>
            
             <!--- get the current status of the shopping cart and return the data to the client --->
            <cfquery name="getCartById" datasource="#application.config.dsn#">
                SELECT     
					format(SUM(price*qty),2) as total,
                    SUM(qty) as itemsincart
                FROM shoppingcartitems
                WHERE idshoppingcart = <cfqueryparam value="#qry_cartExists.idshoppingCart#" cfsqltype="CF_SQL_INTEGER">
            </cfquery>  
            
            <cfset results.qty = getCartById.itemsincart />
            <cfset results.total = getCartById.total />
            <cfset results.idshoppingCart = qry_cartExists.idshoppingCart />
        </cfif>
        <cfreturn results />
    </cffunction>
            
            
    <cffunction name="getShoppingCartDetails" access="remote" returntype="any" output="no" returnFormat="json">
        <cfargument name="storeId" type="any" required="no">
        <cfargument name="sessionId" type="any" required="no"> 
            
        <cfset var results = structNew()>
            
        <!--- check if the cart exists and it is not older than 30 minutes + --->
        <cfquery name="qry_cartExists" datasource="#application.config.dsn#">
            SELECT idshoppingCart
            FROM shoppingcartstatemgr
            WHERE userSessionId = <cfqueryparam value="#arguments.sessionId#" cfsqltype="CF_SQL_VARCHAR">
            AND idstore = <cfqueryparam value="#arguments.storeId#" cfsqltype="CF_SQL_INTEGER">
            AND MINUTE(TIMEDIFF(CURRENT_TIMESTAMP,dateCreated)) < 30
            AND checkoutcompleted = 0;
        </cfquery> 
            
        <cfif qry_cartExists.recordCount EQ 0>
            <cfset results["success"] = false />
        <cfelse>
            
             <!--- get the current status of the shopping cart and return the data to the client --->
            <cfquery name="getCartById" datasource="#application.config.dsn#">
                SELECT 
                    sci.idpost,
                    sci.qty,
                    sci.price,
                    sci.itemdescription as description,
					(SELECT imageName from images where idposts = sci.idpost AND isPrimary = 1 Limit 1) as thumb
                FROM shoppingcartitems sci
                WHERE idshoppingcart = <cfqueryparam value="#qry_cartExists.idshoppingCart#" cfsqltype="CF_SQL_INTEGER">
                AND qty <> 0;
            </cfquery>  
            
            <cfset results = qry2json(querySet=getCartById, lcase=true) />
                
        </cfif>
        <cfreturn results />
    </cffunction>    
	
	<cffunction name="getProdDetailsMetaData" access="public" returntype="any" output="no">
		<cfargument name="postId" required="no">
        <cfargument name="dsn" default="missingDSN"> 
		
		<cfif VAL(arguments.postId EQ 0)>
			<cfreturn>
		</cfif>
		
		
		<!--- test --->
		<cfset var results = structNew()/>
		<cfset var SQLresult = "" />
		<cfset var getImagesByPostId = ""/>
		<cfset results.images = ""/>
	
		<!--- Get post images --->
		<cfquery name="getImagesByPostId" datasource="#arguments.dsn#">
			SELECT  `images`.`idimages`,
				    `images`.`imageName`,
				    `images`.`imagePath`,
				    `images`.`idusers`,
				    `images`.`idposts`,
				    `images`.`imgWidth`,
				    `images`.`imgHeight`,
				    `images`.`idmobileposts`,
				    `images`.`isPrimary`
			FROM  images
			WHERE isDeleted = <cfqueryparam value="0" cfsqltype="CF_SQL_TINYINT">
			AND idposts = <cfqueryparam value="#arguments.postId#" cfsqltype="CF_SQL_INTEGER">
			ORDER BY isPrimary desc;
		</cfquery>
		<cfset results.images = getImagesByPostId />
		
		<!--- get post meta data --->
		<cfquery name="getPostMetaDataById" datasource="#arguments.dsn#">
			SELECT  
			    descriptions,
			    price,
			    subject,
			    isClearance,
                issale,
                inventoryCode,
                quantity,
                pricequalifier,
                availability,
                idstores
			FROM posts
			WHERE idposts = <cfqueryparam value="#arguments.postId#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>
		<cfset results.metaData = getPostMetaDataById />

        <!--- Get Store Policy --->
        <cfquery name="getStorePolicyById" datasource="#arguments.dsn#">
            SELECT policy FROM store_prefs
            WHERE idstores = <cfqueryparam value="#getPostMetaDataById.idstores#" cfsqltype="CF_SQL_INTEGER">
        </cfquery>    
		
        <cfset results.storePolicy = getStorePolicyById.policy />   
		<cfreturn results />
	</cffunction>
		

	<cffunction name="getActiveInventory" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="idStores" required="yes" />
		<cfargument name="searchStr" default="" />
		<cfargument name="categoryId" default="0" />
		<cfargument name="pageIdx" default="0" />
        <cfargument name="itemsPerPage" default="#variables.itemsPerPage#" />
        <cfargument name="dsn" default="missingDSN"/>
        
        
		<cfset var startRecord = 0>
		<cfset var endRecord = 0>
		<cfset var paginationParams = structNew()>
		
		<cfif arguments.pageIdx EQ 0>
			<cfset startRecord = 0>
			<cfset endRecord = arguments.itemsPerPage-1 />
		<cfelse>
			<cfset startRecord = (arguments.pageIdx*arguments.itemsPerPage)>
			<cfset endRecord = startRecord+arguments.itemsPerPage-1 />
		</cfif>
		
		
		<cfset paginationParams._a_startRecord = startRecord>
		<cfset paginationParams._b_endRecord = endRecord>
		<cfset paginationParams.dsn = application.config.dsn />
        
		

	
			<cfset var results=structNew()>
	
			<cfquery name="qry_getPostBySearch" datasource="#argument.dsn#">
				SELECT
					p.idposts,
					p.idusers,
					p.price,
					p.subject,
					-- p.thumb as imageName,
					(SELECT imageName from images WHERE isprimary = 1 AND idposts = p.idposts LIMIT 1) as imageName,
					(SELECT imagePath from images WHERE imageName = p.thumb AND idposts = p.idposts LIMIT 1) as imagePath,
					(SELECT imgWidth from images WHERE imageName = p.thumb AND idposts = p.idposts LIMIT 1) as imageWidth,
					(SELECT imgHeight from images WHERE imageName = p.thumb AND idposts = p.idposts LIMIT 1) as imageHeight,
					(SELECT DATEDIFF(CURRENT_TIMESTAMP,p.dateCreated)) as age,
					(SELECT IF( (DATEDIFF(CURRENT_TIMESTAMP,p.dateCreated) <= sp.newlyArrivedDuration AND sp.showNewBanner = 1) ,true,false)) as showNewBanner,
					p.isClearance,
				
				FROM posts p	
				INNER JOIN store_prefs sp ON sp.idstores = p.idstores
				INNER JOIN stores s ON s.idstores = p.idstores
				WHERE p.status = 3
					AND p.idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
					AND dateExpires  > <cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">
					<cfif arguments.searchStr NEQ "">
						AND (p.subject LIKE ('%#arguments.searchStr#%') OR p.descriptions LIKE ('%#arguments.searchStr#%'))
					</cfif>
					<cfif arguments.categoryId GTE 2>
						AND p.idstoreCategories =  <cfqueryparam value="#arguments.categoryId#" cfsqltype="CF_SQL_INTEGER">
					</cfif>
					<cfif arguments.categoryId EQ 1>
						AND p.isClearance = 1
					</cfif>
				ORDER BY p.dateCreated desc
				LIMIT #startRecord#,#arguments.itemsPerPage#; 
			</cfquery>
                
            <!--- Add the record count to the additional properties being returned --->    
            <cfset paginationParams.recordCount = qry_getPostBySearch.recordCount />       

			<cfif qry_getPostBySearch.recordCount GT 0>	
				<cfset results = qry2json(querySet=qry_getPostBySearch,convertLineBreaksForJSON=true,additionalAttributes=paginationParams)>
			<cfelse>
				<cfset results["success"] = false />
				<cfset results["dsn"] =application.config.dsn />
				<cfset results["RECORDCOUNT"] = qry_getPostBySearch.recordCount />
			</cfif>
			
	

		<cfreturn results>	
	</cffunction>	
	
	
	<cffunction name="getActiveInventory2" access="remote" returntype="any" returnformat="json" output="no">
        <cfargument name="dsn" default="missingDSN"/>
		<cfargument name="idStores" required="yes" />
		<cfargument name="searchStr" default="" />
		<cfargument name="categoryId" default="0" />
		<cfargument name="pageIdx" default="0" />
        <cfargument name="itemsPerPage" default="#variables.itemsPerPage#" />
        
        
		<cfset var startRecord = 0>
		<cfset var endRecord = 0>
		<cfset var paginationParams = structNew()>
		
		<cfif arguments.pageIdx EQ 0>
			<cfset startRecord = 0>
			<cfset endRecord = arguments.itemsPerPage-1 />
		<cfelse>
			<cfset startRecord = (arguments.pageIdx*arguments.itemsPerPage)>
			<cfset endRecord = startRecord+arguments.itemsPerPage-1 />
		</cfif>
		
		
		<cfset paginationParams._a_startRecord = startRecord>
		<cfset paginationParams._b_endRecord = endRecord>
		<cfset paginationParams.dsn = application.config.dsn />
        
		

	
			<cfset var results=structNew()>
			
			<!--- get the current record set to display items --->
			<cfquery name="qry_getPostBySearch" datasource="#arguments.dsn#">
				SELECT
					p.idposts,
					p.idusers,
					p.price,
                    p.small_label,
                    p.small_price,
                    p.large_label,
                    p.large_price,
					p.subject,
                    p.pricequalifier,
                    p.quantity,
                    p.isSale,
					p.descriptions,
                    (SELECT imageName from images WHERE isPrimary = 1 AND idposts = p.idposts LIMIT 1) as imageName,
					(SELECT imagePath from images WHERE isPrimary = 1 AND idposts = p.idposts LIMIT 1) as imagePath, 
					(SELECT imgWidth from images WHERE isPrimary = 1 AND idposts = p.idposts LIMIT 1) as imageWidth, 
					(SELECT imgHeight from images WHERE isPrimary = 1 AND idposts = p.idposts LIMIT 1) as imageHeight, 
					(SELECT DATEDIFF(CURRENT_TIMESTAMP,p.dateCreated)) as age,
					(SELECT IF( (DATEDIFF(CURRENT_TIMESTAMP,p.dateCreated) <= sp.newlyArrivedDuration AND sp.showNewBanner = 1) ,true,false)) as showNewBanner,
					p.isClearance
				FROM posts p	
				LEFT JOIN store_prefs sp ON sp.idstores = p.idstores
				INNER JOIN stores s ON s.idstores = p.idstores
				WHERE p.status = 3
					AND p.idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
                    -- if data expires is null then ignore it.
                    AND (dateExpires IS NULL OR dateExpires  > CURRENT_TIMESTAMP)
					<cfif arguments.searchStr NEQ "">
						AND (p.subject LIKE ('%#arguments.searchStr#%') OR p.descriptions LIKE ('%#arguments.searchStr#%'))
					</cfif>
					<cfif arguments.categoryId GTE 2>
						AND p.idstoreCategories =  <cfqueryparam value="#arguments.categoryId#" cfsqltype="CF_SQL_INTEGER">
					</cfif>
					<cfif arguments.categoryId EQ 1>
						AND p.isClearance = 1
					</cfif>
				ORDER BY p.dateCreated desc
				LIMIT #startRecord#,#arguments.itemsPerPage#; 
			</cfquery>
                
			<!--- get the total count of items for the pagination bar --->
			<cfquery name="qry_getDataSet" datasource="#application.config.dsn#">
				SELECT
					COUNT(p.idposts) as total
				FROM posts p	
				WHERE p.status = 3
					AND p.idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
					AND dateExpires  > <cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">
					<cfif arguments.searchStr NEQ "">
						AND (p.subject LIKE ('%#arguments.searchStr#%') OR p.descriptions LIKE ('%#arguments.searchStr#%'))
					</cfif>
					<cfif arguments.categoryId GTE 2>
						AND p.idstoreCategories =  <cfqueryparam value="#arguments.categoryId#" cfsqltype="CF_SQL_INTEGER">
					</cfif>
					<cfif arguments.categoryId EQ 1>
						AND p.isClearance = 1
					</cfif>
			</cfquery>	
				
            <!--- Add the record count to the additional properties being returned --->    
            <cfset paginationParams.recordCount = qry_getDataSet.total />       

			<cfif qry_getPostBySearch.recordCount GT 0>	
				<cfset results = qry2json(querySet=qry_getPostBySearch,convertLineBreaksForJSON=true,additionalAttributes=paginationParams)>
			<cfelse>
				<cfset results["success"] = false />
				<cfset results["dsn"] =application.config.dsn />
				<cfset results["RECORDCOUNT"] = qry_getPostBySearch.recordCount />
			</cfif>
			
	

		<cfreturn results>	
	</cffunction>	
						
	
	
	<cffunction name="getPostDetails" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="postId" type="string" required="yes">
		<cfargument name="idStores" required="yes">
		
		<cfset var results=structNew()>
		<cfset results["status"] = false>
		<cfset results["checkHost"] = checkHost()>
		
		<cfif checkHost()>
			<cfquery name="qry_getPostDetailsById" datasource="#application.config.dsn#">
				SELECT
					p.idposts,
					p.idusers,
					p.price,
					p.subject,
          			(SELECT CONCAT(p.descriptions,sp.policy)) as descriptions
				FROM posts p
          		INNER JOIN store_prefs sp ON sp.idstores = p.idstores
				WHERE p.idposts = <cfqueryparam value="#arguments.postId#" cfsqltype="CF_SQL_INTEGER">
          		AND p.idstores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>
			<cfset results = qry2json(querySet=qry_getPostDetailsById,convertLineBreaksForJSON=false)>
		</cfif>

		<cfreturn results>	
	</cffunction>
	
	
	<cffunction name="getPostImages" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="postId" type="string" required="yes">
		<cfset var results=structNew()>
		<cfset results["status"] = false>
		
		<cfif checkHost()>
			<cfquery name="qry_getPostImagesById" datasource="#application.config.dsn#">				
				SELECT
						images.imageName,
						images.imagePath,
						images.imgWidth,
						images.imgHeight,
		         (SELECT MAX( images.imgHeight)  FROM    images   WHERE  images.idposts = #arguments.postId#) as maxHeight,
		         (SELECT MAX( images.imgWidth)  FROM    images   WHERE  images.idposts = #arguments.postId#) as maxWidth
						FROM images
						WHERE images.idposts = #arguments.postId#
						AND isDeleted = 0;
			</cfquery>
			<cfset results = qry2json(qry_getPostImagesById)>
		</cfif>

		<cfreturn results>	
	</cffunction>
	
	
	
	
	
	<cffunction name="getCategoriesByStoreId" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="idStores" required="yes" />
        <cfargument name="dsn" default="missingDSN" />
		
<!--- 		<cfoutput>
			SELECT
				idstoreCategories,
				categoryDesc
			FROM storecategories
			WHERE idStores = #arguments.idStores#
			AND isActive = 1
			ORDER BY sortOrder ASC;
		</cfoutput>		
		<cfabort> --->
		
		<cfset var results=structNew()>
		

			
			<!--- get all the distinct custom categories for the items in stock --->	
			<cfquery name="qry_getCategoriesByStoreId" datasource="#arguments.dsn#">
				SELECT
					idstoreCategories,
					categoryDesc
				FROM storecategories
				WHERE idstoreCategories IN (
											SELECT DISTINCT(idstoreCategories) 
											FROM posts
											WHERE status = 3
											AND dateExpires > CURRENT_TIMESTAMP
											AND idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
											)
				AND isActive = <cfqueryparam value="1" cfsqltype="CF_SQL_INTEGER">
				ORDER BY sortOrder ASC, categoryDesc ASC;
			</cfquery>
			
			<!--- Check if there are any items on Clearance --->
			<cfquery name="qry_isClearanceByStoreId" datasource="#arguments.dsn#">
				SELECT idposts
				FROM posts
				WHERE status = 3
				AND dateExpires > CURRENT_TIMESTAMP
				AND idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
				AND isClearance = <cfqueryparam value="1" cfsqltype="CF_SQL_TINYINT">
			</cfquery>
			
			
			<cfif qry_isClearanceByStoreId.recordCount EQ 0>
				<cfset results["showClearance"] = false />
			<cfelse>
				<cfset results["showClearance"] = true />
			</cfif>
			
			<cfset results["customCategories"] = qry2json(qry_getCategoriesByStoreId)>

		
		<cfreturn results>	
	</cffunction>
	
	

	
	
	
	<!--- Used for the storefronts pages --->
	<cffunction name="getLastUpdatedTimeDiff" access="remote" returntype="any" returnformat="json" output="no">
		<cfargument name="idStores" required="yes" />
		
		
		<cfif checkHost()>
			<cfset var qry_getLastUpdatedTimeDiff = ""/>
			<cfset var results=structNew()>
	
			<cfquery name="qry_getLastUpdatedTimeDiff" datasource="#application.config.dsn#">
				SELECT CEIL(MIN(TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP,dateCreated))/60)) as minutesAgo 
				FROM posts p
				WHERE p.status = 3
					AND p.idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
					AND p.dateExpires  > <cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">
			</cfquery>
			


			<cfif qry_getLastUpdatedTimeDiff.minutesAgo NEQ "">	
			
				<cfset results = qry2json(querySet=qry_getLastUpdatedTimeDiff)>
				<cfset var minutesago = qry_getLastUpdatedTimeDiff.minutesAgo />
				
				<cfif minutesago EQ 1>
					<cfset var displaySnippet = "#minutesago# Minute Ago" />
				<cfelse>
					<cfset var displaySnippet = "#minutesago# Minutes Ago" />
				</cfif>
				
				<cfset results["displayLastUpdated"] = true />
				<cfif minutesago GTE 10080 >
					<cfset results["displayLastUpdated"] = false />
				<cfelseif minutesago GTE 1440 >
					<cfset var daysAgo = INT(minutesago/1440)>
					<cfset displaySnippet = "#daysAgo# Days Ago" />
					<cfif daysAgo EQ 1>
						<cfset displaySnippet = "#daysAgo# Day Ago" />
					</cfif>
					
				<cfelseif minutesago GT 59><!--- hours ago --->
					<cfset var hoursAgo =  INT(minutesago/60) />
					<cfset displaySnippet = "#hoursAgo# Hours Ago" />
					<cfif hoursAgo EQ 1>
						<cfset displaySnippet = "#hoursAgo# Hour Ago" />
					</cfif>
				</cfif>
				
			<cfelse>
				<cfset results["success"] = false />
				<cfset results["RECORDCOUNT"] = 0 />
			</cfif>
			
		<cfelse>	
			<cfset results = false>
		</cfif>
		<cfset results["pastTimeFormat"] = displaySnippet />
		<cfreturn results>	
	</cffunction>	
	
	
	<!--- Unknown Uses --->
	<cffunction name="getLastUpdatedTimeDiffJSON" access="remote" returntype="any" returnformat="plain" output="no">
		<cfargument name="idStores" required="yes" />
		<cfargument name="callback" required="yes" />
		
		<cfset var qry_getLastUpdatedTimeDiff = ""/>
		<cfset var dataPacket= "">
		<cfset var results=structNew()>
		
		<cfquery name="qry_getLastUpdatedTimeDiff" datasource="#application.config.dsn#">
			SELECT CEIL(MIN(TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP,dateCreated))/60)) as minutesAgo 
			FROM posts p
			WHERE p.status = 3
				AND p.idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
				AND p.dateExpires  > <cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">
		</cfquery>

		<cfif qry_getLastUpdatedTimeDiff.minutesAgo NEQ "">	
			<!--- <cfset dataPacket = qry2json(querySet=qry_getLastUpdatedTimeDiff)> --->
			<cfset dataPacket = serializeJSON(dataPacket)/>
			<cfset data = arguments.callback & "(" & dataPacket & ")">
			 <cfreturn data>
		<cfelse>
			<cfset results["success"] = false />
			<cfset results["RECORDCOUNT"] = 0 />
			<cfreturn results>	
		</cfif>
	</cffunction>	
	
	<cffunction name="getStoreSliderById" access="remote" returntype="any" returnformat="json" output="yes">
		<cfargument name="idStores" required="yes" />
        <cfargument name="dsn" default="missingDSN" />
		
		<cfquery name="qry_getStoreSliderById" datasource="#arguments.dsn#" result="thisSQL">
			SELECT 
			    `storesliders`.`idstoresliders`,
			    `storesliders`.`idstores`,
			    `storesliders`.`slidemedia`,
			    `storesliders`.`slidepath`,
			    `storesliders`.`mediatext`,
			    `storesliders`.`isVideo`,
			    `storesliders`.`isprimary`,
			    `storesliders`.`isactive`,
				`storesliders`.`link`
			FROM storesliders
			WHERE isActive = <cfqueryparam value="1" cfsqltype="CF_SQL_INTEGER">
			AND idstores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
			ORDER BY isprimary desc;
		</cfquery>
		
		<cfif qry_getStoreSliderById.recordCount GT 0>	
			<cfset results = qry2json(querySet=qry_getStoreSliderById,convertLineBreaksForJSON=true,lcase=true)>
		<cfelse>
			<cfset results["success"] = false />
			<cfset results["RECORDCOUNT"] = qry_getStoreSliderById.recordCount />
		</cfif>
		
		<cfreturn results>	
	</cffunction>	
		
		
	<cffunction name="getStoreMetaById" access="remote" returntype="any" returnformat="json" output="yes">
		<cfargument name="idStores" required="yes" />
        <cfargument name="dsn" default="missingDSN" />
        

		<cfif checkHost()>
			<cfset var results=structNew()>

			<cfquery name="qGetStoreMetaById" datasource="#arguments.dsn#">
				SELECT
					sp.seoKeywords as metaKeyWords,
					sp.seoDesc as metaDesc,
					sp.policy,
					s.storeName,
					s.address,
					s.addr1,
					s.addr2,
					s.city,
					s.state,
					s.zip,
					s.phone,
					s.email,
					s.mapLink,
					s.logoImg,
					s.mapImg,
					s.storeImg,
					s.homePage
				FROM stores s
        		LEFT JOIN store_prefs sp ON sp.idstores = s.idstores
				WHERE s.idstores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
				AND s.isActive = <cfqueryparam value="1" cfsqltype="CF_SQL_INTEGER">
			</cfquery>
	

			<cfif qGetStoreMetaById.recordCount GT 0>	
				<cfset results = qry2json(querySet=qGetStoreMetaById,convertLineBreaksForJSON=true,lcase=true)>
			<cfelse>
				<cfset results["success"] = false />
				<cfset results["RECORDCOUNT"] = qGetStoreMetaById.recordCount />
			</cfif>
			
		<cfelse>	
			<cfset results = false>
		</cfif>

		<cfreturn results>	
	</cffunction>	
	
<!--- http://www.livestorefronts.com/demo/cfc/services.cfc?method=formatTimeDiff&MINUTESAGO=2 --->	
<cffunction name="formatTimeDiff" access="remote" returntype="string">
	<cfargument name="minutesAgo" required="yes">
	
	<cfscript>

		var formatedString = "";

	
		if(minutesAgo < 60){
			//minutes
			
			if(minutesAgo EQ 1){
				formatedString = minutesAgo&' Minute Ago';
			}
			else{
				formatedString = minutesAgo&' Minutes Ago';
			};
		}
		
		else if(minutesAgo GTE 60 AND minutesAgo LT 1440){
			//hours
			var hoursAgo = Int(minutesAgo/60);
			if(hoursAgo EQ 1){
				formatedString = hoursAgo&' Hour Ago';
			}
			else{
				formatedString = hoursAgo&' Hours Ago';
			};
		}
		
		else if(minutesAgo GTE 1440 AND minutesAgo LT 10080){
			//days
			var daysAgo = Int(minutesAgo/60/24);
			if(daysAgo EQ 1){
				formatedString = daysAgo&' Day Ago';
			}
			else{
				formatedString = daysAgo&' Days Ago';
			};
		}else{
			formatedString = '';
		};
		
		
	</cfscript>
	
	<cfreturn formatedString> 

</cffunction>
	
	

	

<!--- Used for the placards --->
<cffunction name="getLastUpdatedJsonp" access="remote" returntype="struct" returnformat="json" output="no">
<cfargument name="idStores" required="yes" />


	<cfif checkHost()>
		<cfset var qry_getLastUpdatedTimeDiff = ""/>
		<cfset var results=structNew()>
	
		<cfquery name="qry_getLastUpdatedTimeDiff" datasource="#application.config.dsn#">
			SELECT CEIL(MIN(TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP,dateCreated))/60)) as minutesAgo 
			FROM posts p
			WHERE p.status = 3
				AND p.idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
				AND p.dateExpires  > <cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">
		</cfquery>
	
		<cfif qry_getLastUpdatedTimeDiff.minutesAgo NEQ "">	
			<cfset results["success"] = true />
			<cfset results["timeInt"] = qry_getLastUpdatedTimeDiff.minutesAgo />
			<cfset results["timeString"] = formatTimeDiff(qry_getLastUpdatedTimeDiff.minutesAgo) />
		<cfelse>
			<cfset results["success"] = false />
			<cfset results["RECORDCOUNT"] = 0 />
		</cfif>
		
	<cfelse>	
		<cfset results = false>
	</cfif>
	
	<cfreturn results>	
</cffunction>	

<!--- Used for the placards --->
<!--- http://www.livestorefronts.com/demo/cfc/services.cfc?method=getStoreURLByStoreId&idStores=2 --->	
<cffunction name="getStoreURLByStoreId" access="remote" returntype="struct" returnformat="json" output="no">
<cfargument name="idStores" required="yes" />

	
	<cfif checkHost()>
		<cfset var thisStoreURL = ""/>
		<cfset var results=structNew()>
		
	
		<cfquery name="qry_getStoreURLById" datasource="#application.config.dsn#">
			SELECT storeURL,type
			FROM stores
			WHERE idStores = <cfqueryparam value="#arguments.idStores#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>
		

	
		<cfif qry_getStoreURLById.storeURL NEQ "">	
			<!--- Send back the url to link to: --->
			<cfset results["storeURL"] = qry_getStoreURLById.storeURL>
			
			<!--- send back the reference to the placard background image --->
			<cfswitch expression="#qry_getStoreURLById.type#">
				<cfcase value="1">
					<!--- Inventory was updated within a week --->
					<cfset results["BGimg"] = "last_updated" />
				</cfcase>
				<cfcase value="2">
					<cfset results["BGimg"] = "ms_LastUpdated_" />
				</cfcase>
				<cfdefaultCase>
					<cfset results["BGimg"] = "last_updated" />
				</cfdefaultcase>
			</cfswitch>
			
			
		<cfelse>
			<cfset results["success"] = false />
			<cfset results["RECORDCOUNT"] = 0 />
		</cfif>
		
	<cfelse>	
		<cfset results = false>
	</cfif>
	
	<cfreturn results>	
</cffunction>	
	

	

<cffunction name="remoteGetLastUpdatedByStoreId" access="remote" returnType="any" returnFormat="plain" output="false">
    <cfargument name="callback" type="string" required="false">
	<cfargument name="storeId" default="0">
	
		
	<cfif arguments.storeId EQ 0>
		<cfreturn>
	</cfif>
	
	
	<cfset var results = structNew() />
	
	<cfset var ageValues = getLastUpdatedJsonp(arguments.storeId) />
	<cfset results["store"] = getStoreURLByStoreId(arguments.storeId)>	

	
	<cfif ageValues.success>
		<cfset results["success"] = true />
		<cfset results["timeLastUpdated"] = ageValues.timeString>
	
		<!--- if the store or garagesale was update more than a week ago. --->
		<!--- Then override the placard background image with a generic one --->
		<cfif ageValues.timeInt GTE 10080>
			<cfset results["store"]["BGimg"] = "1w_updated" />
		</cfif>
	<cfelse>	
		<cfset results["success"] = false />
		<cfset results["timeLastUpdated"] = "" />
		<cfset results["store"]["BGimg"] = "" />
	</cfif>

	
	

    
    <!--- serialize --->
    <cfset results = serializeJSON(results)>
    
    <!--- jsonp wrap --->
    <cfif structKeyExists(arguments, "callback")>
        <cfset results = arguments.callback & "(" & results & ")">
    </cfif>
    
    <cfreturn results>
</cffunction>	

</cfcomponent>