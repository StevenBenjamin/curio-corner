<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<cfcomponent>
	<cfsetting showdebugoutput="false">
	<cffunction name="jsonSecurity" access="private" returntype="any" returnFormat="json">
		<cfargument name="cfctoken" required="no" default=0>
		<cfset var result = false>
		<cfif arguments.cfctoken EQ session.cfctoken>
			<cfset result = true>
		</cfif>
		
		<cfreturn result>
	</cffunction>	
	
	<!--- make sure the request is coming from our website or an authorized host. --->
	<cffunction name="checkHost" access="remote" output="yes" returntype="boolean">
		<cfreturn true>
	</cffunction>
	
	<cffunction name="isEmailBlocked" access="private" returntype="any">
		<cfargument name="email" type="string" required="yes">
						
		<cfset var ipAddress = CGI.remote_addr>
		<cfset var userAgent =LEFT(CGI.http_user_agent,200)>
						
		<cfquery name="chk_email" datasource="#application.config.dsn#">
			SELECT
			`donotemail`.`iddonotemail`,
			`donotemail`.`note`
			FROM `donotemail`
			WHERE email = '#arguments.email#'
		</cfquery>
		
		
		<cfif chk_email.recordCount GT 0>
			<cfmail to="#application.config.adminEmail#"
		        from="server@mapified.com"
		        subject="User Blocked : Email Blocked">
				Time			: #now()#
				ip				: #ipAddress#
				userAgent		: #userAgent#
				note			: #chk_email.note#
				email			: #arguments.email#
			</cfmail>
			
			<cfreturn true>
		</cfif>
		
		<cfreturn false>
	
	</cffunction>
	
	
	
	<cffunction name="isFingerPrintBlocked" access="private" returntype="any">
		
		<cfset var ipAddress = CGI.remote_addr>
		<cfset var userAgent =LEFT(CGI.http_user_agent,200)>
				
		<cfquery name="chk_fingerPrint" datasource="#application.config.dsn#">
		SELECT
			idrestrictedusers,
			`restrictedusers`.`note`
			FROM `restrictedusers`
			WHERE userAgent = <cfqueryparam value="#userAgent#" cfsqltype="CF_SQL_VARCHAR" maxlength="200">
			AND remoteAddr = <cfqueryparam value="#ipAddress#" cfsqltype="CF_SQL_VARCHAR" maxlength="25">
			AND #now()# < dateExpired;
		</cfquery>
		
		
		<cfif chk_fingerPrint.recordCount GT 0>
			<cfmail to="#application.config.adminEmail#"
	        from="server@mapified.com"
	        subject="User Blocked : CheckFingerprint">
				Time			: #now()#
				ip				: #ipAddress#
				userAgent		: #userAgent#
				note			: #chk_fingerPrint.note#
				idrestrictedusers: #chk_fingerPrint.idrestrictedusers#
			</cfmail>
			
			<cfreturn true>
		</cfif>
		
		<cfreturn false>
	
	</cffunction>
	
	<cffunction name="isRemoteAddrBlocked" access="private" returntype="any">
		
		<cfset var ipAddress = CGI.remote_addr>
		<cfset var userAgent =LEFT(CGI.http_user_agent,200)>
				
		<cfquery name="chk_remoteAddr" datasource="#application.config.dsn#">
		SELECT
			idrestrictedusers,
			`restrictedusers`.`note`
			FROM `restrictedusers`
			WHERE remoteAddr = <cfqueryparam value="#ipAddress#" cfsqltype="CF_SQL_VARCHAR" maxlength="25">
			AND #now()# < dateExpired;
		</cfquery>
		
		
		<cfif chk_remoteAddr.recordCount GT 0>
			<cfmail to="#application.config.adminEmail#"
		        from="server@mapified.com"
		        subject="User Blocked : isRemoteAddrBlocked">
				Time			: #now()#
				ip				: #ipAddress#
				userAgent		: #userAgent#
				note			: #chk_remoteAddr.note#
				idrestrictedusers: #chk_remoteAddr.idrestrictedusers#</cfmail>
			
			<cfreturn true>
		</cfif>
		
		<cfreturn false>
	
	</cffunction>
	

	<cffunction name="addUpdateFingerPrint" access="private">
		<cfargument name="idusers" type="numeric" required="yes">
		
			<!--- check if the user is using a different ip address and /or browser --->
				<cfquery name="qry_getUserFingerPrint" datasource="#application.config.dsn#">
					SELECT
						`userfingerprints`.`idfingerPrints`,
						`userfingerprints`.`idusers`,
						`userfingerprints`.`ipAddress`,
						`userfingerprints`.`userAgent`,
						`userfingerprints`.`dateCreated`
						FROM `userfingerprints`
						WHERE idusers = #arguments.idusers#
						AND ipAddress = '#CGI.REMOTE_ADDR#'
						AND userAgent = '#CGI.HTTP_USER_AGENT#';
				</cfquery>
				
				<!--- if the fingerprint exist, update the last used date. --->
				<!--- else add the finger print --->
				
				<cfif qry_getUserFingerPrint.recordCount EQ 0>
					<cfquery name="ins_UserFingerPrint" datasource="#application.config.dsn#">
						INSERT INTO `userfingerprints`
							(
							`idusers`,
							`ipAddress`,
							`userAgent`,
							`dateCreated`,
							`dateLastUsed`)
							VALUES
							(
							<cfqueryparam value="#arguments.idusers#" cfsqltype="CF_SQL_INTEGER">,
							<cfqueryparam value="#LEFT(CGI.REMOTE_ADDR,25)#" cfsqltype="CF_SQL_VARCHAR">,
							<cfqueryparam value="#LEFT(CGI.HTTP_USER_AGENT,250)#" cfsqltype="CF_SQL_VARCHAR">,
							<cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">,
							<cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">
							);
					</cfquery>
				<cfelse>
					<cfquery name="upd_UserFingerPrint" datasource="#application.config.dsn#">
						UPDATE `userfingerprints`
						SET `dateLastUsed` = #now()#
						WHERE idusers = #arguments.idusers#				
					</cfquery>
				</cfif>
	</cffunction>
	
	
	<cffunction name="addUpdateLocation" access="private">
		<cfargument name="idusers" type="numeric" required="yes">
		<cfargument name="latitude" type="numeric" required="yes">
		<cfargument name="longitude" type="numeric" required="yes">
		<cfargument name="geoType" type="string" required="yes">
		<cfargument name="region" type="string" required="yes">
		<cfargument name="accuracy" type="numeric" required="no" default=0>
		
			<!--- check if the user is using a different ip address and /or browser --->
				<cfquery name="qry_getUserLocation" datasource="#application.config.dsn#">
					SELECT
						`userlocations`.`iduserlocations`,
						`userlocations`.`idusers`,
						`userlocations`.`latitude`,
						`userlocations`.`longitude`,
						`userlocations`.`geoType`,
						`userlocations`.`region`,
						`userlocations`.`dateCreated`,
						`userlocations`.`dateLastUsed`
					FROM `userlocations`
					WHERE idusers = #arguments.idusers#
					AND region = '#TRIM(arguments.region)#';
				</cfquery>
				
				<!--- if the fingerprint exist, update the last used date. --->
				<!--- else add the finger print --->
				
				<cfif qry_getUserLocation.recordCount EQ 0>
					<cfquery name="ins_userLocation" datasource="#application.config.dsn#">
						INSERT INTO `userlocations`(
							`idusers`,
							`latitude`,
							`longitude`,
							`geoType`,
							`region`,
							`dateCreated`,
							`dateLastUsed`,
							`accuracy`
							)
							VALUES(
								<cfqueryparam value="#arguments.idusers#" cfsqltype="CF_SQL_INTEGER">,
								<cfqueryparam value="#arguments.latitude#" cfsqltype="CF_SQL_FLOAT">,
								<cfqueryparam value="#arguments.longitude#" cfsqltype="CF_SQL_FLOAT">,
								<cfqueryparam value="#LEFT(arguments.geoType,8)#" cfsqltype="CF_SQL_VARCHAR">,
								<cfqueryparam value="#LEFT(TRIM(arguments.region),200)#" cfsqltype="CF_SQL_VARCHAR">,
								<cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">,
								<cfqueryparam value="#now()#" cfsqltype="CF_SQL_TIMESTAMP">,
								<cfqueryparam value="#arguments.accuracy#" cfsqltype="CF_SQL_INTEGER">
							);
					</cfquery>
				<cfelse>
					<cfquery name="upd_UserLocation" datasource="#application.config.dsn#">
						UPDATE `userlocations`
						SET `dateLastUsed` = #now()#
						WHERE idusers = #arguments.idusers#		
					</cfquery>
				</cfif>
	</cffunction>
	
	

	
    <cffunction name="qry2json" access="public" returntype="any" output="Yes">
        <cfargument name="querySet" type="query" required="yes">
        <cfargument name="convertLineBreaksForJSON" default="false" required="no">
		<cfargument name="escSpecChar" default="false" required="no">
        <cfargument name="startRow" required="no" default="1">
        <cfargument name="endRow" required="yes" default="#arguments.querySet.recordCount#">
		<cfargument name="lcase" type="boolean" default="false">
		<cfargument name="additionalAttributes" type="struct">
        
        <cfset returnedRecordCount = (arguments.endRow-arguments.startRow)+1>
        <cfset  result = structNew()>
		
		<!--- if the querySet is not empty --->
        <cfif querySet.recordCount GT 0>
		<!--- Create an "ROWS" Array to hold the row objects --->
		<cfset ROWS = arrayNew(1)>
				<!---loop through each row in the record set--->
                <cfloop query="querySet" startrow="#arguments.startRow#" endrow="#arguments.endRow#">
					<!--- create an object for each row --->
					<cfset jsonObjItem = structNew()>
					<!--- loop for each column in the datasset --->
                    <cfloop index="columnName" list="#querySet.columnList#" delimiters=",">
						<cfset var thisValueItem = evaluate("querySet."&columnName)>
						<cfif arguments.escSpecChar>
							<cfset thisValueItem = cleanStringForJSON(thisValueItem)>
						</cfif>
						
						<!--- Handle Line Breaks in the value item --->
						<cfif convertLineBreaksForJSON EQ false>
			                <cfset thisValueItemLB = replaceNoCase(thisValueItem,"#chr(13)##chr(10)#","<br>","all")>
			                <cfset thisValueItemLB = replaceNoCase(thisValueItemLB,chr(13),"<br>","all")>
			                <cfset thisValueItemLB = replaceNoCase(thisValueItemLB,chr(10),"<br>","all")>        
			            <cfelse>
			                <cfset thisValueItemLB = replaceNoCase(thisValueItem,"#chr(13)##chr(10)#","\n\r","all")>
			                <cfset thisValueItemLB = replaceNoCase(thisValueItemLB,chr(13),"\n","all")>
			                <cfset thisValueItemLB = replaceNoCase(thisValueItemLB,chr(10),"\r","all")>
			            </cfif>
						
						<!---populate the object with a name:value pair for each column --->
						<cfif arguments.lcase EQ true>
							<cfset jsonObjItem["#lcase(columnName)#"] = thisValueItemLB>
						<cfelse>
							<cfset jsonObjItem["#columnName#"] = thisValueItemLB>
						</cfif>
						
						<!--- add the object to the ROWS array --->
						<cfset ROWS[querySet.currentRow] = jsonObjItem>
                    </cfloop>  
            	</cfloop>
				
			<cfset result.ROWS = ROWS>
			<cfset result.DATASET = INT(variables.returnedRecordCount)>
			<cfset result.RECORDSET = querySet.recordCount> 
			<cfset result["success"] = true>
			<!--- loop over the additionalAttributes and add them to the JSON packet. --->
			<cfif isDefined("additionalAttributes")>
				<cfloop collection="#additionalAttributes#" item="attrItem">
					<cfset result["#attrItem#"] = Evaluate("additionalAttributes.#attrItem#")>
				</cfloop>
			</cfif>
			
			
        <cfelse>
			<cfset result.ROWS = arrayNew(1)>
			<cfset result.DATASET = 0> 
			<cfset result.RECORDSET = querySet.recordCount> 
			<cfset result["success"] = false>
			<cfset result["msg"] = "No records found">
        </cfif>
            
       <cfreturn result>
    </cffunction>
    
    <!--- cleanStringForJSON--->    
    <cffunction name="cleanStringForJSON" access="private" returntype="any" output="No" >
        <cfargument name="rawString" required="Yes" type="string">
    	
		<cfset var specialCharList = "*,-,+">	
		<cfloop index="specChar" list="#specialCharList#">
			<cfset arguments.rawString = replaceNoCase(arguments.rawString,specChar," "&specChar,"all")>
		</cfloop>
	
        <cfreturn arguments.rawString>
    </cffunction>
	
		<!---Stats Functions--->
	<CFFUNCTION name="addStatEvent" access="Public" output="yes" returntype="void">
		<CFARGUMENT name="StatType" type="string" required="Yes" default="">
		<CFARGUMENT name="Details" type="string" required="Yes" default="">
		<CFARGUMENT name="fhid" type="numeric" required="No" default="0"><!--- By FestheadID --->
		<CFARGUMENT name="fid" type="numeric" required="No" default="0"><!--- Affected FestivalID --->

		
		<cfquery name="qstat" datasource="#application.DSN#">
			INSERT INTO fh_Stats (StatTypeID,SessionID,Timestamp,Details,FestheadID,FestivalID)
			SELECT st.StatTypeID
				,<cfqueryparam value="#session.sessionid#" cfsqltype="CF_SQL_VARCHAR"> AS SessionID
				,getdate() AS TimeStamp
				,<cfqueryparam value="#arguments.details#" cfsqltype="CF_SQL_VARCHAR" null="#iif(arguments.details EQ "",true,false)#"> AS Details
				,<cfqueryparam value="#arguments.fhid#" cfsqltype="CF_SQL_INTEGER" null="#iif(arguments.fhid EQ 0,true,false)#"> AS FestheadID
				,<cfqueryparam value="#arguments.fid#" cfsqltype="CF_SQL_INTEGER" null="#iif(arguments.fid EQ 0,true,false)#"> AS FestivalID
			FROM fh_StatTypes st
			WHERE st.StatType = <cfqueryparam value="#arguments.StatType#" cfsqltype="CF_SQL_VARCHAR">
			-- exclude Same StatType for the same festival within 24 hours for same session
			AND (SELECT COUNT(*) FROM fh_Stats WHERE StatTypeID=st.StatTypeID AND SessionID=<cfqueryparam value="#session.sessionid#" cfsqltype="CF_SQL_VARCHAR"> AND FestivalID=<cfqueryparam value="#arguments.fid#" cfsqltype="CF_SQL_INTEGER"> AND DATEDIFF(HH,Timestamp,GETDATE()) < 24) = 0
		</cfquery>
	</CFFUNCTION>
	
	<cffunction name="getSiteModeByCode" access="private" returntype="string">
		<cfargument name="siteModeCode" type="any" required="yes">
			
		<cfswitch expression="#arguments.siteModeCode#">
			<cfcase value="2784673898">
				<cfset var siteMode = "demo">
			</cfcase>
			
			<cfcase value="5632470820">
				<cfset var siteMode = "live">
			</cfcase>
			
			<cfcase value="4983672625">
				<cfset var siteMode = "comingSoon">
			</cfcase>
		</cfswitch>
		
		<cfreturn siteMode>
	</cffunction>
	
	 <!--- http://www.bennadel.com/blog/149-Ask-Ben-Converting-A-Query-To-A-Struct.htm --->
	<cffunction name="QueryToStruct" access="private" returntype="any" output="false" 
    hint="Converts an entire query or the given record to a struct. This might return a structure (single record) or an array of structures.">

    <!--- Define arguments. --->
    <cfargument name="Query" type="query" required="true" />
    <cfargument name="Row" type="numeric" required="false" default="0" />

	    <cfscript>
	
		    // Define the local scope.
		    var LOCAL = StructNew();
		
		    // Determine the indexes that we will need to loop over.
		    // To do so, check to see if we are working with a given row,
		    // or the whole record set.
		    if (ARGUMENTS.Row){
		
		    // We are only looping over one row.
		    LOCAL.FromIndex = ARGUMENTS.Row;
		    LOCAL.ToIndex = ARGUMENTS.Row;
		
		    } else {
		
		    // We are looping over the entire query.
		    LOCAL.FromIndex = 1;
		    LOCAL.ToIndex = ARGUMENTS.Query.RecordCount;
		
		    }
		
		    // Get the list of columns as an array and the column count.
		    LOCAL.Columns = ListToArray( ARGUMENTS.Query.ColumnList );
		    LOCAL.ColumnCount = ArrayLen( LOCAL.Columns );
		
		    // Create an array to keep all the objects.
		    LOCAL.DataArray = ArrayNew( 1 );
		
		    // Loop over the rows to create a structure for each row.
		    for (LOCAL.RowIndex = LOCAL.FromIndex ; LOCAL.RowIndex LTE LOCAL.ToIndex ; LOCAL.RowIndex = (LOCAL.RowIndex + 1)){
		
		    // Create a new structure for this row.
		    ArrayAppend( LOCAL.DataArray, StructNew() );
		
		    // Get the index of the current data array object.
		    LOCAL.DataArrayIndex = ArrayLen( LOCAL.DataArray );
		
		    // Loop over the columns to set the structure values.
		    for (LOCAL.ColumnIndex = 1 ; LOCAL.ColumnIndex LTE LOCAL.ColumnCount ; LOCAL.ColumnIndex = (LOCAL.ColumnIndex + 1)){
		
		    // Get the column value.
		    LOCAL.ColumnName = LOCAL.Columns[ LOCAL.ColumnIndex ];
		
		    // Set column value into the structure.
		    LOCAL.DataArray[ LOCAL.DataArrayIndex ][ LOCAL.ColumnName ] = ARGUMENTS.Query[ LOCAL.ColumnName ][ LOCAL.RowIndex ];
		
		    }
		
		    }
		
		
		    // At this point, we have an array of structure objects that
		    // represent the rows in the query over the indexes that we
		    // wanted to convert. If we did not want to convert a specific
		    // record, return the array. If we wanted to convert a single
		    // row, then return the just that STRUCTURE, not the array.
		    if (ARGUMENTS.Row){
		
		    // Return the first array item.
		    return( LOCAL.DataArray[ 1 ] );
		
		    } else {
		
		    // Return the entire array.
		    return( LOCAL.DataArray );
		
		    }
	
	    </cfscript>
    </cffunction>
	
	
	
	<!--- http://www.bennadel.com/blog/124-Ask-Ben-Converting-a-Query-to-an-Array.htm --->
	<cffunction name="QueryToArray" access="private" returntype="array" output="false"
    hint="This turns a query into an array of structures.">

    <!--- Define arguments. --->
    <cfargument name="Data" type="query" required="yes" />

    <cfscript>

    // Define the local scope.
    var LOCAL = StructNew();

    // Get the column names as an array.
    LOCAL.Columns = ListToArray( ARGUMENTS.Data.ColumnList );

    // Create an array that will hold the query equivalent.
    LOCAL.QueryArray = ArrayNew( 1 );

    // Loop over the query.
    for (LOCAL.RowIndex = 1 ; LOCAL.RowIndex LTE ARGUMENTS.Data.RecordCount ; LOCAL.RowIndex = (LOCAL.RowIndex + 1)){

    // Create a row structure.
    LOCAL.Row = StructNew();

    // Loop over the columns in this row.
    for (LOCAL.ColumnIndex = 1 ; LOCAL.ColumnIndex LTE ArrayLen( LOCAL.Columns ) ; LOCAL.ColumnIndex = (LOCAL.ColumnIndex + 1)){

    // Get a reference to the query column.
    LOCAL.ColumnName = LOCAL.Columns[ LOCAL.ColumnIndex ];

    // Store the query cell value into the struct by key.
    LOCAL.Row[ LOCAL.ColumnName ] = ARGUMENTS.Data[ LOCAL.ColumnName ][ LOCAL.RowIndex ];

    }

    // Add the structure to the query array.
    ArrayAppend( LOCAL.QueryArray, LOCAL.Row );

    }

    // Return the array equivalent.
    return( LOCAL.QueryArray );

    </cfscript>
    </cffunction>
	
	
	<cffunction name="decryptMerchantPassword" access="private" returnType="string">
		<cfargument name="pw" default="">
	
		<cfset var thisPW = TRIM(arguments.pw)>
		
		<!--- base64 decode --->
		<!--- http://www.bennadel.com/blog/1343-Converting-A-Base64-Value-Back-Into-A-String-Using-ColdFusion.htm --->
		<cfset var decryptStr = URLDecode(ToString(ToBinary(thisPW)))>
		
		<!--- strip out the preceeding "The" --->
		<cfset decryptStr = MID(decryptStr,4,LEN(decryptStr))>
		
		<!--- strip out the appended "!" --->
		<cfset decryptStr = LEFT(decryptStr,LEN(decryptStr)-1)>
		
		<cfreturn decryptStr>
	</cffunction>
	

	
</cfcomponent>