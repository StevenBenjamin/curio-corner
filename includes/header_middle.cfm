<section class="h_bot_part container head-menu">
	<div class="clearfix row headerPadding">
		<div class="col-xs-12 lg-no-gutter">
	
            <cfoutput>
                <div class="col-xl-9 col-md-3 col-sm-4 col-xs-8 col-xxs-12 t_xs_align_c">
					<div class="row">
						<div class="col-sm-12 p-left_0">
		                    <a href="" onclick="return false;" class="logo m_xs_bottom_15 d_xs_inline_b align_left-xl align_left-xs">
		                        <img src="#RawMetaData.rows[1].logoimg#" alt="#RawMetaData.rows[1].storeName# logo">
		                    </a>
						</div>
						<div class="visible-xl visible-xs col-xs-12 mTop_12 p-left_0 addressFont">
							<div class="block-addr">
								<div class="align_left-xl align_left-xs xl-mr_10 xs-mr_10">#RawMetaData.rows[1].storeName#</div>
			                	<div class="align_left-xl align_left-xs xl-mr_10 xs-block">#RawMetaData.rows[1].addr1#</div>
								<div style="clear:both;"></div>
							</div>
							<div>
				                <cfif LEN(RawMetaData.rows[1].addr2)>
									<div class="align_left-xl align_left-xs xl-mr_10 xs-mr_10">#RawMetaData.rows[1].addr2#</div>
								</cfif>
			                	<div class="align_left-xl align_left-xs xs-block">#RawMetaData.rows[1].city# #RawMetaData.rows[1].state# #RawMetaData.rows[1].zip#</div>
							</div>
						</div>
					</div><!--- row --->
                </div>

                <div class="col-xl-5 col-md-6 col-sm-4 hidden-xl hidden-xs t_xs_align_c t_lg_align_c t_xs_paddingB align_left-xl xl-fs_16 xl-mTop_21">    
                    <div class="align_left-xl xl-mr_10 xl-ml_10">#RawMetaData.rows[1].storeName#</div>
                    <div class="align_left-xl xl-mr_10">#RawMetaData.rows[1].addr1#</div>
                    <cfif LEN(RawMetaData.rows[1].addr2)><div class="align_left-xl xl-mr_10">#RawMetaData.rows[1].addr2#</div></cfif>
                    <div class="align_left-xl">#RawMetaData.rows[1].city# #RawMetaData.rows[1].state# #RawMetaData.rows[1].zip#</div>
                </div>

                <div class="col-xl-3 col-md-3 col-sm-4 col-xs-4 col-xxs-12 xxs-mTop_12 p-right_0">
					<div class="row">
						 <div class="col-sm-12 p-right_0 xxs-tAlign-c">
		                     <dl class="l_height_medium">    
		                         <dd class="f_size_ex_large color_dark">
								 	<span class="callUs">Call Us: &nbsp;</span>
									<a href="tel:1#RawMetaData.rows[1].phone#"><b>#RawMetaData.rows[1].phone#</b></a>
								</dd>
		                     </dl>
		                 </div>
						 
		                 <div class="col-sm-12 xxs-tAlign-c">
		                     #RawMetaData.rows[1].email#
		                 </div>
						 
						 <div class="col-sm-12 relative type_2 p-right_0 mTop_5" role="search">
		                      <input type="text" placeholder="Search" name="search" id="freeTextSearch" class="r_corners f_size_medium full_width">
		                      <button class="f_right search_button tr_all_hover f_xs_none" onclick="Search.doSearch()">
		                          <img src="../assets/images/search-icon.png" alt="" class="img-responsive searchIcon" border="0">
		                      </button>
		                  </div>
					 </div>
                </div>
            </cfoutput>
        </div>    
	</div><!--- row --->

</section>
