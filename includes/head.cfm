
<!-- Title here -->
<title>Chiu Quon</title>
<!-- Description, Keywords and Author -->
<meta name="description" content="Your description">
<meta name="keywords" content="Your,Keywords">
<meta name="author" content="ResponsiveWebInc">

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Styles -->
<!-- Bootstrap CSS -->
<link href="/globalStoreAssets/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/globalStoreAssets/assets/css/bootstrap-theme.min.css" rel="stylesheet">
<link href="/globalStoreAssets/assets/css/jquery.datetimepicker.css" rel="stylesheet">
<link href="/globalStoreAssets/assets/css/sweet-alert.css" rel="stylesheet">




<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link href="/globalStoreAssets/assets/css/settings.css" rel="stylesheet">		
<!-- FlexSlider Css -->
<link rel="stylesheet" href="/globalStoreAssets/assets/css/flexslider.css" media="screen" />
<!-- Portfolio CSS -->
<link href="/globalStoreAssets/assets/css/prettyPhoto.css" rel="stylesheet">
<!-- Font awesome CSS -->
<!--- <link href="css/font-awesome.min.css?ver=10001" rel="stylesheet"> --->	
<!-- Custom Less -->
<link href="/globalStoreAssets/assets/css/less-style.css" rel="stylesheet">	
<!-- Custom CSS -->
<link href="/globalStoreAssets/assets/css/style_theme2.css" rel="stylesheet">
<!--[if IE]><link rel="stylesheet" href="css/ie-style.css"><![endif]-->


<!--- Development diagnostics --->
<link href="/globalStoreAssets/assets/css/bootstrap_3.2_xxs_diagnostics.css" rel="stylesheet">



<!-- favIcon -->
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">