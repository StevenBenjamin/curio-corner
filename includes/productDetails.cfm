
<!--- Get the assets required for the product details popup --->
<cfinvoke component="assets.cfc.services" method="getProdDetailsMetaData" returnvariable="productDetailsAssets">
    <cfinvokeargument name="postId" value="#VAL(url.postId)#">
    <cfinvokeargument name="dsn" value="#url.dsn#">
</cfinvoke> 


<cfoutput>
	<div class="popup_wrap d_none" id="quick_view_product">
		<section class="popup r_corners shadow">
			<button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"></button>
			<span class="closeDetails" onclick="Popup.closePopup()"><img src="globalStoreAssets/assets/images/close-icon-16.png" alt="" width="16" height="16" border="0"></span>
			<div class="clearfix">
				<div>
					<!--left popup column-->
					<div class="f_left half_column_left47">
						<div class="relative d_inline_b qv_preview">
							<!--- <span class="hot_stripe"><img src="images/sale_product.png" alt=""></span> --->
							<img src="#productDetailsAssets.Images.IMAGEPATH#details360/#productDetailsAssets.Images.IMAGENAME#" class="tr_all_hover" alt="">
						</div>
					</div>
					<!--right popup column-->
					<div class="f_right half_column_right53">
						<!--description-->
						<h2 class="m_bottom_10"><a href="##" onclick="return false;" class="color_dark fw_medium">#productDetailsAssets.metadata.subject#</a></h2>
				
					
						
						<hr class="divider_type_3 m_bottom_10">
						<cfif LEN(productDetailsAssets.metadata.descriptions)>
							<p class="m_bottom_10">#productDetailsAssets.metadata.descriptions#</p>
							<hr class="divider_type_3 m_bottom_15">
						</cfif>
                        
						<cfif isDefined("productDetailsAssets.storePolicy") AND LEN(productDetailsAssets.storePolicy)>
							<p class="m_bottom_10">#productDetailsAssets.storePolicy#</p>
							<hr class="divider_type_3 m_bottom_15">
						</cfif>
                        
                        
                        <div class="m_bottom_15">
                            <span>Availability:</span>&nbsp;
                            <span><span class="color_green">in stock</span>&nbsp;&nbsp;&nbsp;&nbsp;Qty: #productDetailsAssets.metadata.quantity#</span>
                        </div>
                        
                        <hr class="m_bottom_10 divider_type_3">
						<div class="m_bottom_15">
							<!--- <s class="v_align_b f_size_ex_large">$152.00</s> --->
                            <span class="v_align_b f_size_big scheme_color fw_medium">
                                #dollarFormat(productDetailsAssets.metadata.price)# 
                            </span>
                            <cfif LEN(TRIM(productDetailsAssets.metadata.priceQualifier))><span class="priceQualifier">/#productDetailsAssets.metadata.priceQualifier#</span></cfif>
						</div>
                        
                        
                        <!--- Tile Thumbs --->
						<cfif productDetailsAssets.Images.recordCount GT 1>
							<div class="thumbsGrid">
		                        <div class="m_bottom_3">
									<cfloop query="#productDetailsAssets.Images#" startRow="1" endrow="4">
										 <span data-src="#productDetailsAssets.Images.IMAGEPATH#details360/#productDetailsAssets.Images.IMAGENAME#">
										 	<img src="#productDetailsAssets.Images.IMAGEPATH#Thumbs/#productDetailsAssets.Images.IMAGENAME#" alt="">
										 </span>
									</cfloop>
		                       </div>
		                       <div class="m_bottom_12">
								 	<cfloop query="#productDetailsAssets.Images#" startRow="5" endrow="8">
										 <span data-src="#productDetailsAssets.Images.IMAGEPATH#details360/#productDetailsAssets.Images.IMAGENAME#">
										 	<img src="#productDetailsAssets.Images.IMAGEPATH#Thumbs/#productDetailsAssets.Images.IMAGENAME#" alt="">
										 </span>
									</cfloop>
		                       </div>
						   </div>
					   </cfif>
                       
                        
                      
                 		<!--- Shoping cart --->
						<!--- 
						<div class="clearfix m_bottom_15">
							<button class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large">Add to Cart</button>
							<button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0"><i class="fa fa-heart-o f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
							<button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0"><i class="fa fa-files-o f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>
							<button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0 relative"><i class="fa fa-question-circle f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Ask a Question</span></button>
						</div>
						--->
						
						
					</div>
				</div>
			</div>
		</section>
	</div>
</cfoutput>