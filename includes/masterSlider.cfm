<!-- masterslider -->
<cfif arrayLen(variables.storeSlider.rows)>
    <div class="master-slider ms-skin-default xss_hideSlider" id="masterslider">
        <cfoutput>
            <cfloop array="#variables.storeSlider.rows#" index="sliderItem">
                <div class="ms-slide">
                    <img src="globalStoreAssets/assets/images/blank.gif" data-src="#sliderItem.slidepath##sliderItem.slidemedia#" alt=""/>     
                
                    <cfif LEN(sliderItem.mediatext) >
                        <!--- slide text layer --->
                        <div class="ms-layer ms-caption" style="top:10px; left:30px;">
                            #sliderItem.mediatext#
                        </div>
                    </cfif>
                    
                    <cfif LEN(sliderItem.link) AND sliderItem.isVideo >
                        <!--- youtube video --->
                        <a href="#sliderItem.link#" data-type="video">Youtube video</a>
                    <cfelse>
                        <!--- linked slide --->
                        <a href="#sliderItem.link#" target="_blank">Link</a>
                    </cfif> 
                </div>
            </cfloop>    
        </cfoutput>
    </div>
</cfif>    
<!-- end of masterslider -->