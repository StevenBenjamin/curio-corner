<section class="nav-menu-wrap type_2 relative clearfix t_xs_align_c">
		<!--- Hamburger button for responsive menu --->
		<button id="nav-menu-button" class="r_corners centered_db d_none tr_all_hover d_xs_block">
			Main&nbsp;Menu
		</button>
        <div class="clearfix"></div>
		<!--- main menu --->
		<nav module="navigation" class="f_left f_xs_none d_xs_none t_xs_align_l">	
			<ul class="horizontal_list nav-main-menu drop-down-menu clearfix">
				<!--- Home --->
                <li class="relative f_xs_none m_xs_bottom_5">
                    <a href="index.cfm" class="tr_delay_hover nav-color-light tt_uppercase singleLine"><b>Home</b></a>
				</li>
                <!--- Search By Category --->
				<li class="relative f_xs_none m_xs_bottom_5">
                    <a href="" onclick="return false;" class="tr_delay_hover color_light tt_uppercase doubleLine">
                        <b>Search By</br/>Category</b>
                    </a>  
                    <div class="nav-sub-menu-wrap top_arrow d_xs_none tr_all_hover clearfix r_corners w_xs_auto categoryContainer">
                        <!--- Create a grid with n columns by 7 rows --->
                        <cfset variables.rowCounter = 0/>      
                        <cfoutput>
                            <cfloop array="#variables.storeCategories.customCategories.rows#" index="categoryItem">
                               <div class="f_left f_xs_none">
                                    <ul class="sub_menu first categoryGroup">
                                        <li><a class="color_dark tr_delay_hover" href="index.cfm?category=#URLEncodedFormat(variables.categoryItem.CATEGORYDESC,'utf-8')#">#variables.categoryItem.CATEGORYDESC#</a></li>
                                    </ul>
                                </div>
                                <cfset variables.rowCounter = variables.rowCounter+1/>  
                            </cfloop>
                        </cfoutput>
                        <div class="clearfix"></div>
                    </div>
				</li>
                    
				<li class="relative f_xs_none m_xs_bottom_5">
                    <a href="index.cfm?category=art" class="tr_delay_hover color_light tt_uppercase singleLine">
                        <b>Art</b>
                    </a>
				</li>
				<li class="relative f_xs_none m_xs_bottom_5">
                    <a href="index.cfm?category=Jewelry" class="tr_delay_hover color_light tt_uppercase singleLine">
                        <b>Jewelry</b>
                    </a>
				</li>
				<li class="relative f_xs_none m_xs_bottom_5">
                    <a href="index.cfm?category=Furniture" class="tr_delay_hover color_light tt_uppercase singleLine">
                        <b>Furniture</b>
                    </a>
				</li>
				<li class="relative f_xs_none m_xs_bottom_5">
                    <a href="index.cfm?category=toys" class="tr_delay_hover color_light tt_uppercase singleLine">
                        <b>Toys</b>
                    </a>
                </li>
				<li class="relative f_xs_none m_xs_bottom_5">
                    <a href="index.cfm?category=Food" class="tr_delay_hover color_light tt_uppercase singleLine">
                        <b>Food</b>
                    </a>
                </li>
			</ul>
		</nav>
    
		<cfif dayAgoBadge.displayLastUpdated>
	    	<div class="lastUpdatedCircle">
				<div class="clockIcon"><img class="img-responsive" src="../assets/images/clock-icon.png" alt="" border="0"></div>
				<div class="lastUpdatedtextWrapper">
					<div class="lastUpdatedTitle">Updated</div>
					<div class="lastUpdatedTime"><cfoutput>#dayAgoBadge.pastTimeFormat#</cfoutput></div>
				</div>
			</div>
		</cfif>
		<!--- <cfinclude template="shopingCartMenuItems.cfm" /> --->
	</section>