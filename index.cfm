<!doctype html>
<!--- 2 = Live Storefronts demo --->
<!--- You have to se this for index.cfm & search.cfm --->

<cfinclude template="storeId.cfm"/>



<cfset variables.datasource = application.datasource />
<cfinclude template="../All_Stores/assets/includes/dataModelRequests2.cfm">


<!--- get initial page load inventory. --->
<!--- <cfset initActiveInventory = application.services.getActiveInventory(variables.storeId,"",variables.categoryDesc,0,25)/> --->

<cfif NOT isStruct(variables.RawMetaData) AND variables.RawMetaData EQ false>
	No Store Data Available:<br/>
	<cfdump var="#RawMetaData#"/>
	<cfabort>
</cfif>



<cfset variables.metaData = RawMetaData.Rows[1]>


<!--- Set the store policy to the session scope because it is used for each product details request --->
<cfset session.storePolicy = variables.metaData.policy />


<!--- <cfdump var="#RawMetaData#"/> --->
<!--- aborted
<cfabort> --->


<!--[if IE 9 ]><html class="ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->


	<head>
        <cfoutput>
		    <title></title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
            <!--meta info-->
            <meta name="author" content="Live Storefronts">
            <meta name="keywords" content="#RawMetaData.rows[1].metakeywords#">
            <meta name="description" content="#RawMetaData.rows[1].metadesc#">
        </cfoutput>
        <!-- favIcon -->
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
		
		
		<script>
		  WebFontConfig = {
		    google: {
      				families: ['Roboto:n3']
    			}
		  };
		
		  (function() {
		    var wf = document.createElement('script');
		    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		              '://ajax.googleapis.com/ajax/libs/webfont/1.5.6/webfont.js';
		    wf.type = 'text/javascript';
		    wf.async = 'true';
		    var s = document.getElementsByTagName('script')[0];
		    s.parentNode.insertBefore(wf, s);
		  })();
		</script>
		
		
		
		<!--stylesheet include-->
		<link rel="stylesheet" type="text/css" media="all" href="globalStoreAssets/assets/css/bootstrap.css?v=1.00" />
        <link rel="stylesheet" type="text/css" media="all" href="globalStoreAssets/assets/masterslider/style/masterslider.css?v=1.00" />
        <link rel="stylesheet" type="text/css" media="all" href="globalStoreAssets/assets/masterslider/skins/default/style.css?v=1.00" />
	
		<!--font include-->
        <link rel="stylesheet" type="text/css" media="all" href="assets/stylesheets/site.css">
		<script src="globalStoreAssets/assets/scripts/lib/modernizr.js"></script>
	</head>
	<body onload="doOnload()">
		<div class="topBar"></div>
		<!--- http://detectmobilebrowser.com/mobile --->
		<!--- Show fewer items per a page for mobile apps --->
		<cfif reFindNoCase("(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino",CGI.HTTP_USER_AGENT) GT 0 OR reFindNoCase("1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-",Left(CGI.HTTP_USER_AGENT,4)) GT 0>
	<cfset variables.itemsperPage = 6 />
</cfif>
		<div class="topSpacer"></div>
        <!--- This is on the development branch 101010 --->
		<!--boxed layout  [boxed_layout | wide_layout]-->
		<div class="boxed_layout relative w_xs_auto xs_noGutter main_layout">


			<!--- markup header --->
            <cfinclude template="includes/header_middle.cfm"/>

			<cfinvoke component="assets.cfc.services" method="getLastUpdatedTimeDiff" returnvariable="dayAgoBadge">
				<cfinvokeargument name="idStores" value="#variables.storeId#">
			</cfinvoke>
										
            <div class="boxed_layout relative w_xs_auto head-menu">
                <header module="navContainer">
                    <cfinclude template="includes\mainMenu.cfm" />
                </header>
            </div>


			<!--slider hi-->
	        <!--- the the main slider  --->
            <cfif NOT findNoCase("category",CGI.QUERY_STRING) AND NOT findNoCase("searchstr",CGI.QUERY_STRING)>
                <cfinclude template="includes/masterSlider.cfm" />
            </cfif>
            
            <div class="container main-page-container">
                <!--content-->
                        <cfinclude template="./includes/productsMenu.cfm" />
                        <!--products-->
                        <div class="products_append_container"></div>
                        <section class="products_container clearfix m_bottom_25 m_sm_bottom_15">
                            <!--- Send back HTML headers and any other available HTML --->
                            <cfflush>

                            <cfinvoke component="assets.cfc.services" method="getActiveInventory2" returnvariable="initActiveInventory">
                                <cfinvokeargument name="dsn" value="#application.datasource#">
                                <cfinvokeargument name="idStores" value="#variables.storeId#">
                                <cfinvokeargument name="searchStr" value="#variables.searchStr#">
                                <cfinvokeargument name="categoryId" value="#variables.categoryDesc#">
                                <cfinvokeargument name="pageIdx" value="0">
                                <cfinvokeargument name="itemsPerPage" value="#variables.itemsperPage#">   
                            </cfinvoke>
							
                            <cfif initActiveInventory.recordCount>
                                <cfoutput>
                                    <cfloop array="#initActiveInventory.ROWS#" index="variables.item">
                                        <div class="product_item">
                                            <cfif variables.item.age LTE 3>
                                                <div class="productBannerWrapper">
                                                    <div class="productBanner">Just Arrived</div>
                                                </div>  
                                            </cfif>         
                                            <figure class="r_corners photoframe productfigure shadow relative hit animate_ftb long animate_vertical_finished">
                                                
												<!--product preview-->
                                                <a href="" onclick="return false;" class="d_block relative pp_wrap">
                                                    <!--hot product A-->
                                                    <!--- <span class="hot_stripe"><img src="images/hot_product.png" alt=""></span> --->
                                                    <img src="#variables.item.IMAGEPATH#Details360/#variables.item.IMAGENAME#" class="tr_all_hover" alt="">
                                                    <span onclick="Popup.getDetailsByPostId(#variables.item.idposts#)" class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                                                </a>
                                                <!--description and price of product-->
                                                <figcaption>
                                                    <h5 class="m_bottom_10"><a href="##" onclick="return false;" class="color_dark">#replaceNoCase(variables.item.SUBJECT,"|","<br/>")#</a></h5>
                                                    <div class="clearfix">
                                                        <span class="scheme_color f_left f_size_large m_bottom_15">#dollarFormat(variables.item.PRICE)#</span>
                                    <cfif LEN(variables.item.pricequalifier)>&nbsp;/#variables.item.pricequalifier#</cfif>
                                    <cfif VAL(variables.item.quantity) GT 1>&nbsp;&nbsp;Qty:&nbsp;#variables.item.quantity#</cfif>
                                                    </div>
                                                    <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0" data-popup="##quick_view_product">Details</button>
                                                </figcaption>
                                            </figure>
											
											<!--- add to cart banner --->
											<!---
											<div class="item-hover br-red hidden-xs"></div>
											<a class="link hidden-xs" href="##">Add to cart</a>
											--->
                                    
                                        </div><!--- Product Item --->
                                    </cfloop>
                                </cfoutput>
                            <cfelse>
                                <div class="product_item">
									<br/>
                                    <h2 class="tt_uppercase m_bottom_20 color_dark heading1">No Products Found</h2>
                                </div>
                            </cfif>
                            <cfflush>
                        </section> <!--- End of Products --->
                    </div>
                
                <!--markup footer-->
                <!--- <cfinclude template="includes\footer.html" /> --->
            <!--social widgets-->
            <!--- <cfinclude template="includes/socialWidget.html" /> --->

            <!--Product Detail Popup-->
            <div id="popupContainer"></div>
            <!--- <cfinclude template="includes/productDetailPopup.html" /> --->


            <!--- jQuery Templates --->
            <cfinclude template="../All_Stores/assets/includes/jQueryTemplates4.cfml">


            <!--- Go to top button --->
            <button class="t_align_c r_corners tr_all_hover animate_ftl" id="go_to_top">
                <i class="fa fa-angle-up"></i>
            </button>
             


		<!--scripts include-->
        <!--- Set the dsn for the client to pass back to the ajax services --->        
        <script>gdsn = <cfoutput>'#variables.datasource#'</cfoutput>;</script>   
    
		<script src="globalStoreAssets/assets/scripts/lib/jquery-2.1.0.min.js"></script>
		<script src="globalStoreAssets/assets/scripts/lib/jquery-migrate-1.2.1.min.js"></script>
        <script src="globalStoreAssets/assets/scripts/lib/jquery.tmpl.min.js"></script>
        <script src="globalStoreAssets/assets/scripts/utils_003.js"></script>
		<script src="globalStoreAssets/assets/scripts/lib/retina.js"></script>
		<script src="globalStoreAssets/assets/scripts/lib/jquery.easing.1.3.js"></script>
		<script src="globalStoreAssets/assets/scripts/lib/waypoints.min.js"></script>
		<script src="globalStoreAssets/assets/scripts/lib/jquery.isotope.min.js"></script>
        <script src="globalStoreAssets/assets/masterslider/masterslider.min.js"></script>
		<!--- <script src="globalStoreAssets/assets/scripts/lib/jquery.custom-scrollbar.js"></script> --->
        <script src="assets/scripts/site.min.js"></script>
        <!--- <script src="js/owl.carousel.min.js"></script> --->
		<!--- <script src="js/jquery.tweet.min.js"></script> --->
        <script>
		
			
            $(document).ready(function(){

                Layout.init();
    
                var slider = new MasterSlider();
                slider.setup('masterslider' , {
                        width:1220,    // slider standard width
                        height:350,   // slider standard height
                        space:5,
                        layout: "fillwidth",
                        loop: true,
                        preload: 1,
                        fillMode: "fill",
                        wheel: false,
                        autoplay: false,
                        endPause: false,
                        overPause: true
                    });
                    // adds Arrows navigation control to the slider.
                    slider.control('arrows');
				
                
				
            }); //ready
			
			var doOnload = function(){
				setTimeout(delayedPageLoad,1000);
				function delayedPageLoad(){
					ProductService.init(<cfoutput>#variables.storeId#</cfoutput>);
				};
				
			};
				
				
        </script>
        <!--- <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5306f8f674bfda4c"></script> --->
	   </div><!--- main layout --->
    </body>
</html>
